/*
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   lexi.c
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Lexical Conventions. */
/** @page lexi Lexical Conventions 

    The compiler is case insensitive, except for labels. 

    These are the the keywords of the language 
    @verbatim 
    ADDRESS	ALTRO		BASELINE	BROADCAST	
    BUFFER      CONFIG		COUNTER		END		
    ERROR	FILTER		FIRST		GROUP
    INCREMENT	INVERT		JUMP		K1		
    K2		K3		L1		L2
    L3		LOOP		PATH		PEDESTAL
    POWER	PRE		RCU		READ
    READOUT	RESET           SAVE            SECOND
    SIZE        SOFTWARE        STATUS          SUPPRESSION
    TRIGGER     WAIT            WRITE           ZERO
    @endverbatim 

    Label names have the for of C identifiers, that is a letter or and
    underscore followed by 0 or more letters, numbers or underscores. 

    Numbers are always integers and can be written in decimal or
    hexadecimal notation.   Number are never signed, with the
    exception of the relative address argument to the @c JUMP or 
    @c LOOP instructions 
*/
/*
 * EOF
 */

