/*
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   fmd.c
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Forward Multiplicity Detector Instructions. */
/** @page fmd Forward Multiplicity Detector

    FMD (Forward Multiplicity Detector) instructions are preceeded by
    the keyword @c FMD.  

    All FMD @c READ instructions read the data from the FMD's into
    the RCU result memory.   

    The FMD instructions are 


    @par WRITE BIAS @< @a chip_address | BROADCAST @> @a value [@a value2](@c BIAS)
    Write the bias voltage for upper and lower half of the hybrid
    cards.  If @a value2 is given, then it's the voltage for the upper
    part, otherwise @a value is used for both halfs.   

    The bias voltage has a range of 0 - -1.5V, so each step is
    approximently -0.0059V.  To calculate the voltage @f$ V@f$ for a
    given input number use @f$ V= N/256 \times (-1.5)@f$ where @f$
    N@f$ is the input number.  To calculate the number @f$ N@f$
    corresponding to a voltage use @f$ N = V / 1.5 \times 256@f$,
    where again, @f$ V@f$ is the voltage.  The default value is 
    @f$ -1V \rightarrow 170@f$ (or @c 0xaa in hexidecimal).

    @par READ BIAS @< @a chip_address  @> (@c BIAS)
    Read the bias of a board. 

    @par WRITE VFP @< @a chip_address | BROADCAST @> @a value [@a value2](@c VFP)
    Write the VA VFP voltage (control voltage to feedback resistance
    in pre-amplifier) for upper and lower half of the hybrid
    cards.  If @a value2 is given, then it's the voltage for the upper
    part, otherwise @a value is used for both halfs. 

    The VFP voltage has a range of -0.2 - -1V, so each step is
    approximently -0.0031V.  To calculate the voltage @f$ V@f$ for a
    given input number use @f$ V= N/256 \times (-1 + 0.2) - 0.2@f$
    where @f$ N@f$ is the input number.  To calculate the number @f$
    N@f$ corresponding to a voltage use 
    @f$ N = (V + 0.2) / (-1 + 0.2) \times 256@f$, where again, 
    @f$ V@f$ is the voltage.  The default value is 
    @f$ -0.38V \rightarrow 58@f$ (or @c 0x3a in hexidecimal).

    @par READ VFP @< @a chip_address  @> (@c VFP)
    Read the vfp of a board. 

    @par WRITE VFS @< @a chip_address | BROADCAST @> @a value [@a value2](@c VFS)
    Write the VA VFS voltage (control voltage to feedback resistance
    in shaper-amplifier) for upper and lower half of the hybrid
    cards.  If @a value2 is given, then it's the voltage for the upper
    part, otherwise @a value is used for both halfs. 

    The VFP voltage has a range of -0.55 - +0.43V, so each step is
    approximently +0.0038V.  To calculate the voltage @f$ V@f$ for a
    given input number use @f$ V= N/256 \times (0.43 + 0.55) - 0.55@f$ 
    where @f$ N@f$ is the input number.  To calculate the number 
    @f$ N@f$ corresponding to a voltage use 
    @f$ N = (V + 0.55) / (0.43 + 0.55) \times 256@f$, where again, 
    @f$ V@f$ is the voltage.  The default value is 
    @f$ -0.03V \rightarrow 136 @f$ (or @c 0x88 in hexidecimal).

    @par READ VFS @< @a chip_address  @> (@c VFS)
    Read the vfs of a board. 

    @par WRITE PULSER @< @a chip_address | BROADCAST @> @a value (@c PULSER)
    Write the VA PULSER voltage for the hybrid
    cards.
    
    The pulser voltage has a range of  0 - +0.11V, so each step is
    approximently +0.0004V.  To calculate the voltage @f$ V@f$ for a
    given input number use @f$ V= N/256 \times (0.11)@f$ 
    where @f$ N@f$ is the input number.  To calculate the number 
    @f$ N@f$ corresponding to a voltage use 
    @f$ N = V / 0.11 \times 256@f$, where again, @f$ V@f$ is the 
    voltage.  The default  value is 
    @f$ 0 \rightarrow 0@f$ (or @c 0x00 in hexidecimal).

    @par READ PULSER @< @a chip_address  @> (@c PULSER)
    Read the vfs of a board. 

    @par WRITE SHIFT CLOCK @< @a chip_address | BROADCAST @> @a div [@a phase] (@c SHIFT)
    Write the VA shift clock division and phase for the hybrid
    cards.
    @par READ SHIFT CLOCK @< @a chip_address  @> (@c SHIFT)
    Read the shift of a board. 

    @par WRITE SAMPLE CLOCK @< @a chip_address | BROADCAST @> @a div [@a phase] (@c SAMPLE)
    Write the ALTRO sample clock division and phase for the hybrid
    cards.
    @par READ SAMPLE CLOCK @< @a chip_address  @> (@c SAMPLE)
    Read the sample of a board. 

    @par WRITE HOLD DELAY @< @a chip_address | BROADCAST @> @a value (@c HOLDD)
    Write the VA hold delay time (in terms a 40MHz clock) from Level
    0 trigger for the hybrid cards.  That is, the number of clock
    cycles to wait from receiving the level trigger 0 until the hold
    signal is asserted.    

    The level 0 trigger arrives at the digitizer board @f$
    1.2\mu{}s@f$ after the bunch crossing.  The specs of the VA1 chips
    states that it has a shaping time of @f$ 1.5 \mu{}s@f$, so we need
    to wait @f$ 300ns@f$.  Since each clock cycle is @f$ 50ns@f$, we
    need to wait 6 clock cycles.  Hence the default value is @c 0x0006

    @par READ HOLD DELAY @< @a chip_address  @> (@c HOLDD)
    Read the hold delay of a board. 

    @par WRITE TRIGGER ONE TIMEOUT @< @a chip_address | BROADCAST @> @a value (@c L1TO)
    Write the VA trigger one timeout time (in terms a 40MHz clock) from Level
    0 trigger for the hybrid cards.  That is, how many clock cycles we
    need to wait from receiving the level 0 trigger before we discard
    the trigger, unless of course we get a level 1 trigger. 

    The level 0 trigger arrives at the digitizer board @f$
    1.2\mu{}s@f$ after the bunch crossing.  In the DAQ TDR, it is
    stated that the level 1 trigger arrives at the @f$ 6.5 \mu{}s@f$
    after the bunch crossing, so we should time out @f$ 5.3\mu{}s@f$
    after reciving the level 0 trigger. Since each clock cycle is 
    @f$ 50ns@f$, we need to wait 106 clock cycles.  Hence the default
    value is @c 0x006a 

    @par READ TRIGGER ONE TIMEOUT @< @a chip_address  @> (@c L1TO)
    Read the trigger one timeout of a board. 

    @par WRITE TRIGGER TWO TIMEOUT @< @a chip_address | BROADCAST @> @a value (@c L2TO)
    Write the VA trigger two timeout time (in terms a 40MHz clock) from Level
    0 trigger for the hybrid cards. That is, how many clock cycles we
    need to wait from receiving the level 0 trigger before the ALTRO
    discards the trigger, unless of course we get a level 2 trigger.

    Note, that this register is only used when we generate triggers on
    the board via the @c FMD @c TRIGGER instruction.  It is not used
    by the normal trigger handler. 

    The level 0 trigger arrives at the digitizer board @f$
    1.2\mu{}s@f$ after the bunch crossing.  In the DAQ TDR, it is
    stated that the level 2 trigger arrives at the @f$ 88 \mu{}s@f$
    after the bunch crossing, so we should time out @f$ 86.2\mu{}s@f$
    after reciving the level 0 trigger. Since each clock cycle is 
    @f$ 50ns@f$, we need to wait 1736 clock cycles.  Hence the default
    value is @c 0x06c8 


    @par READ TRIGGER TWO TIMEOUT @< @a chip_address  @> (@c L2TO)
    Read the trigger two timeout of a board. 

    @par WRITE RANGE @< @a chip_address | BROADCAST @> @a lower @a upper(@c RANGE)
    Write the VA range of strips to read out.  @a lower should be in
    the range @f$ [0,127]@f$ and @a upper should be in the range @f$
    [lower, 127]@f$.  Note, that if @f$ lower = upper@f$ then that
    strip is left open all the time.  This is useful for
    calibrations. 
    @par READ TRIGGER ZERO @< @a chip_address  @> (@c L0CNT)
    Read the number of recieved trigger 0's

    @par READ TRIGGER ONE @< @a chip_address  @> (@c L0ACC)
    Read the number of accepted trigger 0's. 

    @par CHANGE VOLTAGE @< @a chip_address  | BROADCAST@> (@c CHVOL)
    Change the voltages on the hybrids.   This changes the voltages to
    the hybrid card to the values currently in the registers on the
    board controller.   

    Normally, one would do something like 
    @verbatim
    GROUP change_dacs : 
       Write FMD BIAS   broadcast 0xaa
       Write FMD VFP    broadcast 0x3a
       Write FMD VFS    broadcast 0x88
       Write FMD PULSER broadcast 0x00
       FMD Change Voltage broadcast
    END
    @endverbatim 

    @par TRIGGER @< @a chip_address  | BROADCAST@> (@c FMDTRIG)
    Start a trigger sequence.  First, a level 0 trigger is made on the
    chip, and propageted to the normal trigger handling.  Then after
    (TRIGGER ONE TIMEOUT - 1) clock cycles, a level 1 trigger is
    generated and propegated to the normal trigger handler and to the
    ALTRO's on the board.   Finally, after (TRIGGER TWO TIMEOUT - 2)
    clock cycles a level 2 trigger is generated and sent to the
    ALTRO's on the board.  

    @par PULSER MODE  @< ON | OFF @> (@c CALMODE)
    Turn calibration mode on or off.  When this mode is on, the VA1
    chips does not read input from the sensor, but rather from the
    calibration DAC.  

    So when the chip recieves a levvel 0 trigger (either from an
    external source, or from the internal trigger box), the trigger
    handler push some charge on to the input of the VA1 chips.   That
    means, that the trigger handler has to wait an additional 
    @f$ 1.2\mu{}s@f$ before issuing the hold signal, to allow the VA1
    shaper circuit to shape the signal.  The additional delay is
    currently hard-coded into the chip. 

*/
/*
 * EOF
 */
