/*
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   bc.c
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Board Controller Instructions. */
/** @page bc Board Controller Instructions.

    BC (board controller) instructions are preceeded by the keyword @c BC. 

    All BC @c READ instructions read the data from the BC's into
    the RCU result memory.   

    The BC instructions are 

    @par WRITE TEMPERATURE THRESHOLD @< @a chip_address | BROADCAST @> @a value(@c T_TH)
    Write the temperature threshold.  Default is @f$ 40^\circ C@f$
    which is equivilant to @c 0xA0.  That is, each increment is @f$
    1/4^\circ C@f$. 

    @par READ TEMPERATURE THRESHOLD @< @a chip_address @>(@c T_TH) 
    Read the temperature threshold of a board. 

    @par WRITE ANALOG VOLTAGE THRESHOLD @< @a chip_address | BROADCAST @> @a value(@c AV_TH)
    Write the analog voltage threshold.  Default is @f$ 3.61 V@f$
    which is equivilant to @c 0x330.  That is, each increment is
    roughly @f$ 4.43 mV@f$.  


    @par READ ANALOG VOLTAGE THRESHOLD @< @a chip_address (@c AV_TH) @>
    Read the analog voltage threshold of a board. 

    @par WRITE ANALOG CURRENT THRESHOLD @< @a chip_address | BROADCAST @> @a value(@c AC_TH)
    Write the analog current threshold.  Default is @f$ 0.75 A@f$
    which is equivilant to @c 0x2C. That is, each increment is
    roughly @f$ 17 mA@f$.  


    @par READ ANALOG CURRENT THRESHOLD @< @a chip_address (@c AC_TH) @>
    Read the analog current threshold of a board. 

    @par WRITE DIGITIAL VOLTAGE THRESHOLD @< @a chip_address | BROADCAST @> @a value(@c DV_TH)
    Write the digitial voltage threshold.  Default is @f$ 2.83 V@f$
    which is equivilant to @c 0x280. That is, each increment is
    roughly @f$ 4.43 mV@f$.  


    @par READ DIGITIAL VOLTAGE THRESHOLD @< @a chip_address (@c DV_TH) @>
    Read the digitial voltage threshold of a board. 

    @par WRITE DIGITIAL CURRENT THRESHOLD @< @a chip_address | BROADCAST @> @a value(@c DC_TH)
    Write the digitial current threshold.  Default is @f$ 1.92 A@f$
    which is equivilant to @c 0x40. That is, each increment is
    roughly @f$ 30 mA@f$.  


    @par READ DIGITIAL CURRENT THRESHOLD @< @a chip_address (@c DC_TH) @>
    Read the digitial current threshold of a board. 

    @par READ TEMPERATURE @< @a chip_address (@c TEMP) @>
    Read the temperature  of a board.  One count is @f$ 0.25^\circ C@f$
    @par READ ANALOG VOLTAGE @< @a chip_address (@c AV) @>
    Read the analog voltage  of a board. One count is @f$ 4.43 mV@f$
    @par READ ANALOG CURRENT @< @a chip_address (@c AC) @>
    Read the analog current  of a board. One count is @f$ 17 mA@f$
    @par READ DIGITAL VOLTAGE @< @a chip_address (@c DV) @>
    Read the digital voltage  of a board. One count is @f$ 4.43 mV@f$
    @par READ DIGITAL CURRENT @< @a chip_address (@c DC) @>
    Read the digital current  of a board. One count is @f$ 30 mA@f$
    @par READ TRIGGER ONE COUNTER @< @a chip_address (@c L1CNT) @>
    Read the trigger one counter  of a board. 
    @par READ TRIGGER TWO COUNTER @< @a chip_address (@c L2CNT) @>
    Read the trigger two counter  of a board. 
    @par READ SAMPLE CLOCK COUNTER @< @a chip_address (@c SLCKCNT) @>
    Read the sample clock counter  of a board. 
    @par READ DATA STROBE COUNTER @< @a chip_address (@c DSTBCNT) @>
    Read the data strobe counter  of a board. 

    @par WRITE TEST MODE WORDS @< @a chip_address | BROADCAST @> @a value(@c TSMWORD)
    Write the test mode words.  Default is @c 0x1

    @par READ TEST MODE WORDS @< @a chip_address  @> (@c TSMWORD)
    Read the test mode words of a board. 

    @par WRITE UNDER SAMPLE @< @a chip_address | BROADCAST @> @a value(@c USRATIO)
    Write the under sample.  Default is @c 0x1

    @par READ UNDER SAMPLE @< @a chip_address  @> (@c USRATIO)
    Read the under sample of a board. 

    @par WRITE ZERO CONFIG @< @a chip_address | BROADCAST @> @a value(@c CSR0)
    Write the zero config.  Default is @c 0x3FF.  
    <table>
      <tr><th>bits</th><th>Flag</th>
      <tr><td>0-7</td><td>Interrupt mask</td></tr>
      <tr><td>8-9</td><td>Error mask</td></tr>
      <tr><td>10</td><td>enable for continues mode. </td></tr>
    </table>

    @par READ ZERO CONFIG @< @a chip_address  @> (@c CSR0)
    Read the zero config of a board. 

    @par WRITE FIRST CONFIG @< @a chip_address | BROADCAST @> @a value(@c CSR1)
    Write the first config (error flags).  Default is @c ?
    <table>
      <tr><th>bit</th><th>Flag</th>
      <tr><td>0</td><td>temperature over threshold</td></tr>
      <tr><td>1</td><td>analog voltage over threshold</td></tr>
      <tr><td>2</td><td>analog current over threshold</td></tr>
      <tr><td>3</td><td>digital voltage over threshold</td></tr>
      <tr><td>4</td><td>digital current over threshold</td></tr>
      <tr><td>5</td><td>PASA power supply error</td></tr>
      <tr><td>6</td><td>ALTRO power supply error</td></tr>
      <tr><td>7</td><td>missed sample clocks</td></tr>
      <tr><td>8</td><td>parity error</td></tr>
      <tr><td>9</td><td>instruction error</tr>
      <tr><td>10</td><td>ALTRO error flag</td></tr>
      <tr><td>11</td><td>Slow control error flag</td></tr>
      <tr><td>12</td><td>General error</td></tr>
      <tr><td>13</td><td>interrupt</td></tr>
    </table>
    @par READ FIRST CONFIG @< @a chip_address  @> (@c CSR1)
    Read the first config of a board. 

    @par WRITE SECOND CONFIG @< @a chip_address | BROADCAST @> @a value(@c CSR2)
    Write the second config.  Default is @c 0xF.  
    <table>
      <tr><th>bit(s)</th><th>Flag</th>
      <tr><td>0</td><td>ALTRO switch</td></tr>
      <tr><td>1</td><td>PASA switch</td></tr>
      <tr><td>2</td><td>Readout clock enable</td></tr>
      <tr><td>3</td><td>ADC clock enable</td></tr>
      <tr><td>4-5</td><td>(Test mode) ADC address</td></tr>
      <tr><td>6-8</td><td>(Test mode) ALTRO address</td></tr>
      <tr><td>9</td><td>(Test mode) continous test mode<td></tr>
      <tr><td>10</td><td>(Test mode) card isolated<td></tr>
      <tr><td>11-15</td><td>Card address<td></tr>
    </table>
    @par READ SECOND CONFIG @< @a chip_address  @> (@c CSR2)
    Read the second config of a board. 

    @par WRITE THIRD CONFIG @< @a chip_address | BROADCAST @> @a value(@c CSR3)
    Write the third config.  Default is @c 0x2220.  
    <table>
      <tr><th>bit(s)</th><th>Flag</th>
      <tr><td>0-7</td><td>Read-out / sample clock warning ratio</td></tr>
      <tr><td>8-14</td><td>ALTRO master watch dog</td></tr>
      <tr><td>15</td><td>Conversion end</td></tr>
    </table>
    @par READ THIRD CONFIG @< @a chip_address  @> (@c CSR3)
    Read the third config of a board. 


*/
/*
 * EOF
 */
