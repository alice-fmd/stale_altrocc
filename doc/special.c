/*
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   special.c
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Specials Instructions. */
/** @page special Special Instructions

    There are two special instructions: The @c LOOP and the @c JUMP
    instructions. 

    @par JUMP @e address
    
    Execution will be transfered immediately to the instruction
    address given by @e address.   

    @par LOOP @e address @e number 

    Exection will be transfered immediately to the instruction
    address given by @e address for as long as the number of times
    this instruction was seen is less than @e number.   Note, that it
    only makes sense to @c LOOP to a previous instruction address
    (this is not diagnosed). 

    @e address my be an absolute instruction address, in which case
    the argument should be an unsigned integer.  

    If address is a literal plus (@c +) or minus (@c -), followed by
    an unsigned integer, then the address is relative to the current
    address.  Note, that the compiler will check if you jump before
    address 0, but will not check if you jump beyond the end of the
    instruction sets loaded.  This is due to the single pass of the
    compiler. 

    If the address is an identifier, it is taken to be the label of an
    instruction.   The compiler will try to resolve this label.  If
    the label was not previsouly set, then the instruction is illegal,
    and the compiler aborts. 


*/
/*
 * EOF
 */
