/*
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file    grammar.c
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jan 26 16:56:54 2005
    @brief   Informative grammar specification
*/
/** @page app_grammar Informative grammar specification
    
    This is an informative grammar for the RCU.  It is no where
    authorative. Please also refer to parser.y 

    @section grammar_lexi Lexical conventions 
    @note The quotes are not part of the literal symbols. 
    @verbatim 
      INTEGER:     Either a decimal or hexideximal whole positive number 
      PLUS:        Literal "+"
      MINUS:       Literal "-" 
      COLON:       Literal ":"
      NAME:        [_a-zA-Z][_a-zA-Z0-9]* 

      ZERO         The string "ZERO" in any case (including mixed)
      WRITE        The string "WRITE" in any case (including mixed)
      WORDS        The string "WORDS" in any case (including mixed)
      WAIT         The string "WAIT" in any case (including mixed)
      VOLTAGE      The string "VOLTAGE" in any case (including mixed)
      VFS          The string "VFS" in any case (including mixed)
      VFP          The string "VFP" in any case (including mixed)
      UNDER        The string "UNDER" in any case (including mixed)
      TWO          The string "TWO" in any case (including mixed)
      TRIGGER      The string "TRIGGER" in any case (including mixed)
      THRESHOLD    The string "THRESHOLD" in any case (including mixed)
      TIMEOUT      The string "TIMEOUT" in any case (including mixed)
      THIRD        The string "THIRD" or "3rd" in any case (including mixed)
      TEMPERATURE  The string "TEMPERATURE" in any case (including mixed)
      TEST         The string "TEST" in any case (including mixed)
      SUPPRESSION  The string "SUPPRESSION" in any case (including mixed)
      STROBE       The string "STROBE" in any case (including mixed)
      STATUS       The string "STATUS" in any case (including mixed)
      SOFTWARE     The string "SOFTWARE" in any case (including mixed)
      SIZE         The string "SIZE" in any case (including mixed)
      SHIFT        The string "SHIFT" in any case (including mixed)
      SECOND       The string "SECOND" or "2nd" in any case (including mixed)
      SCAN         The string "SCAN" in any case (including mixed)
      SAVE         The string "SAVE" in any case (including mixed)
      SAMPLE       The string "SAMPLE" in any case (including mixed)
      RESET        The string "RESET" in any case (including mixed)
      READOUT      The string "READOUT" in any case (including mixed)
      READ         The string "READ" in any case (including mixed)
      RCU          The string "RCU" in any case (including mixed)
      RANGE        The string "RANGE" in any case (including mixed)
      PULSER       The string "PULSER" in any case (including mixed)
      PRE          The string "PRE" in any case (including mixed)
      POWER        The string "POWER" in any case (including mixed)
      PEDESTAL     The string "PEDESTAL" in any case (including mixed)
      PATH         The string "PATH" in any case (including mixed)
      ONE          The string "ONE" in any case (including mixed)
      ON           The string "ON" in any case (including mixed)
      OFF          The string "OFF" in any case (including mixed)
      MONITOR      The string "MONITOR" in any case (including mixed)
      MODE         The string "MODE" in any case (including mixed)
      LOOP         The string "LOOP" in any case (including mixed)
      L3           The string "L3" in any case (including mixed)
      L2           The string "L2" in any case (including mixed)
      L1           The string "L1" in any case (including mixed)
      LENGTH       The string "LENGTH" in any case (including mixed)
      LATCH        The string "LATCH" in any case (including mixed)
      K3           The string "K3" in any case (including mixed)
      K2           The string "K2" in any case (including mixed)
      K1           The string "K1" in any case (including mixed)
      JUMP         The string "JUMP" in any case (including mixed)
      INVERT       The string "INVERT" in any case (including mixed)
      INCREMENT    The string "INCREMENT" in any case (including mixed)
      HOLD         The string "HOLD" in any case (including mixed)
      GROUP        The string "GROUP" in any case (including mixed)
      FIRST        The string "FIRST" or "1st" in any case (including mixed)
      FILTER       The string "FILTER" in any case (including mixed)
      FMD          The string "FMD" in any case (including mixed)
      EVENT        The string "EVENT" in any case (including mixed)
      ERROR        The string "ERROR" in any case (including mixed)
      END          The string "END" in any case (including mixed)
      DATA         The string "DATA" in any case (including mixed)
      DIGITAL      The string "DIGITAL" in any case (including mixed)
      DELAY        The string "DELAY" in any case (including mixed)
      CURRENT      The string "CURRENT" in any case (including mixed)
      COUNTER      The string "COUNTER" in any case (including mixed)
      CONFIG       The string "CONFIG" in any case (including mixed)
      CLOCK        The string "CLOCK" in any case (including mixed)
      CHANGE       The string "CHANGE" in any case (including mixed)
      BUFFER       The string "BUFFER" in any case (including mixed)
      BROADCAST    The string "BROADCAST" in any case (including mixed)
      BIAS         The string "BIAS" in any case (including mixed)
      BC           The string "BC" in any case (including mixed)
      BASELINE     The string "BASELINE" in any case (including mixed)
      ANALOG       The string "ANALOG" in any case (including mixed)
      ALTRO        The string "ALTRO" in any case (including mixed)
      ADDRESS      The string "ADDRESS" in any case (including mixed)
    @endverbatim 

    @section grammar_blocks Blocks
    @verbatim 
      input	
        : 
        | input group		
        ;
      
      group	
        : GROUP list labeled_end	
        ;
      
      labeled_end
        : END
        | NAME COLON END
        ;
      
      list	
        : labeled_instruction
        | list labeled_instruction
        ;
      
      labeled_instruction
        : instruction
        | NAME COLON instruction
        ;
    @endverbatim

    @section grammar_instr Instructions 
    @verbatim       
      instruction
        : loop_instruction
        | jump_instruction
        | RCU rcu_instruction
        | ALTRO altro_instruction
        | BC bc_instruction
        | FMD fmd_instruction
        ;
      
      loop_instruction
        : LOOP  address INTEGER
        ;
      jump_instruction
        : JUMP 	address
        ;
    @endverbatim

    @section grammar_rcu_instr RCU Instructions 
    @verbatim       
      rcu_instruction
        : reset_instruction
        | readout_instruction
        | pedestal_instruction
        | wait_instruction
        | trigger_wait_instruction
        ;
      
      reset_instruction
        : RESET STATUS
        | RESET TRIGGER CONFIG
        | RESET TRIGGER COUNTER
        ;
      readout_instruction
        : READOUT
        ;
      
      pedestal_instruction
        : READ  PEDESTAL INTEGER
        | WRITE PEDESTAL INTEGER
        | WRITE PEDESTAL BROADCAST
        ; 
      wait_instruction
        : WAIT INTEGER
        ;
      trigger_wait_instruction
        : TRIGGER WAIT
        ;
      
      address
        : INTEGER
        | PLUS INTEGER
        | MINUS INTEGER
        | NAME
        ;
    @endverbatim

    @section grammar_altro_instr ALTRO Instructions 
    @verbatim       
      altro_instruction
        : k_instruction
        | l_instruction
        | vfped_instruction
        | pmdta_instruction
        | adevl_instruction
        | zsthr_instruction
        | bcthr_instruction
        | trcfg_instruction
        | dpcfg_instruction
        | bfnpt_instruction
        | erstr_instruction
        | trcnt_instruction
        | pmadd_instruction
        | wpinc_instruction
        | rpinc_instruction
        | chrdo_instruction
        | swtrg_instruction
        | trclr_instruction
        | erclr_instruction
        ;
      
      bcast_local_address
        : BROADCAST
        | chip_address channel_address
        ;
      
      bcast_or_chip_address
        : BROADCAST
        | chip_address
        ;
      
      chip_address
        : INTEGER
        ;
      
      channel_address 
        : INTEGER
        ;      
    @endverbatim

    @subsection grammar_local_instr Channel Specific ALTRO Instructions 
    @verbatim       
      k_instruction
        : READ K1 bcast_local_address
        | READ K2 bcast_local_address
        | READ K3 bcast_local_address
        | WRITE K1 bcast_local_address INTEGER
        | WRITE K2 bcast_local_address INTEGER
        | WRITE K3 bcast_local_address INTEGER
        ;
      
      l_instruction
        : READ L1 bcast_local_address
        | READ L2 bcast_local_address
        | READ L3 bcast_local_address
        | WRITE L1 bcast_local_address INTEGER
        | WRITE L2 bcast_local_address INTEGER
        | WRITE L3 bcast_local_address INTEGER
        ;
      
      vfped_instruction
        : READ FIRST BASELINE bcast_local_address
        | WRITE FIRST BASELINE bcast_local_address INTEGER
        ;
      
      pmdta_instruction
        : READ PEDESTAL bcast_local_address
        | WRITE PEDESTAL bcast_local_address INTEGER
        ;      
    @endverbatim

    @subsection grammar_global_instr Global ALTRO Instructions 
    @verbatim       
      zsthr_instruction
        : READ ZERO SUPPRESSION bcast_or_chip_address
        | WRITE ZERO SUPPRESSION bcast_or_chip_address INTEGER INTEGER
        ;
      
      bcthr_instruction
        : READ SECOND BASELINE bcast_or_chip_address
        | WRITE SECOND BASELINE bcast_or_chip_address INTEGER INTEGER
        ;
      
      trcfg_instruction
        : READ TRIGGER CONFIG bcast_or_chip_address
        | WRITE TRIGGER CONFIG bcast_or_chip_address INTEGER INTEGER
        ;
      
      dpcfg_instruction
        : READ  FIRST PATH bcast_or_chip_address
        | WRITE FIRST PATH bcast_or_chip_address invert_opt first_baseline_opt 
      		     second_baseline_opt zero_suppression_opt
        ;
      
      invert_opt
        : 
        | INVERT
        ;
      
      first_baseline_opt
        : 
        | FIRST BASELINE INTEGER
        ;
      
      second_baseline_opt
        : 
        | SECOND BASELINE INTEGER INTEGER
        ;
      
      zero_suppression_opt
        : 
        | ZERO SUPPRESSION INTEGER INTEGER INTEGER   
        ;
      
      bfnpt_instruction
        : READ  SECOND PATH bcast_or_chip_address 
        | WRITE SECOND PATH bcast_or_chip_address pretrigger_opt
                            buffer_size_opt filter_enable_opt power_save_opt
        ;
      
      pretrigger_opt
        : 
        | PRE TRIGGER INTEGER
        ;
      
      buffer_size_opt
        : 
        | BUFFER SIZE INTEGER
        ;
      
      filter_enable_opt
        : /* Empty */
        | FILTER
        ;
      
      power_save_opt
        : /* Empty */
        | POWER SAVE
       
        ;
      
      pmadd_instruction
        : READ  PEDESTAL ADDRESS bcast_or_chip_address
        | WRITE PEDESTAL ADDRESS bcast_or_chip_address INTEGER
        ;
      
      erstr_instruction
        : READ ERROR chip_address
        ;
      
      adevl_instruction
        : READ ADDRESS chip_address channel_address 
        ;
      
      trcnt_instruction
        : READ TRIGGER COUNTER bcast_or_chip_address
        ;
    @endverbatim

    @subsection grammar_command_instr ALTRO Commands
    @verbatim       
      wpinc_instruction
        : WRITE INCREMENT bcast_or_chip_address
        ;
      
      rpinc_instruction
        : READ INCREMENT bcast_or_chip_address
        ;
      
      chrdo_instruction
        : READOUT chip_address channel_address
        ;
      
      swtrg_instruction
        : SOFTWARE TRIGGER bcast_or_chip_address
        ;
      
      trclr_instruction
        : RESET TRIGGER COUNTER bcast_or_chip_address
        ;
      
      erclr_instruction
        : RESET ERROR bcast_or_chip_address
        ;      
      @endverbatim

      @subsection grammar_bc_instr BC instructions 
      @verbatim 
        bc_instruction
          : temperature_threshold_instruction
          | analog_voltage_threshold_instruction
          | analog_current_threshold_instruction
          | digital_voltage_threshold_instruction
          | digital_current_threshold_instruction
          | temperature_instruction
          | analog_voltage_instruction
          | analog_current_instruction
          | digital_voltage_instruction
          | digital_current_instruction
          | l1cnt_instruction
          | l2cnt_instruction
          | sclkcnt_instruction
          | dstbcnt_instruction
          | tsmword_instruction
          | usratio_instruction
          | csr_instruction
          | cntlat_instruction
          | cntclr_instruction
          | csr1clr_instruction
          | alrst_instruction
          | bcrst_instruction
          | stcnv_instruction
          | scevl_instruction
          | evlrdo_instruction
          | sttsm_instruction
          | acqrdo_instruction
          ;
        
        temperature_threshold_instruction
          : READ TEMPERATURE THRESHOLD chip_address 
          | WRITE TEMPERATURE THRESHOLD bcast_or_chip_address INTEGER
          ; 
        analog_voltage_threshold_instruction
          : READ ANALOG VOLTAGE THRESHOLD chip_address 
          | WRITE ANALOG VOLTAGE THRESHOLD bcast_or_chip_address INTEGER
          ;  
        analog_current_threshold_instruction
          : READ ANALOG CURRENT THRESHOLD chip_address 
          | WRITE ANALOG CURRENT THRESHOLD bcast_or_chip_address INTEGER
          ;  
        digital_voltage_threshold_instruction
          : READ DIGITAL VOLTAGE THRESHOLD chip_address 
          | WRITE DIGITAL VOLTAGE THRESHOLD bcast_or_chip_address INTEGER
          ;  
        digital_current_threshold_instruction
          : READ DIGITAL CURRENT THRESHOLD chip_address 
          | WRITE DIGITAL CURRENT THRESHOLD bcast_or_chip_address INTEGER
          ;  
        temperature_instruction
          : READ TEMPERATURE chip_address
          ; 
        analog_voltage_instruction
          : READ ANALOG VOLTAGE chip_address
          ;  
        analog_current_instruction
          : READ ANALOG CURRENT chip_address
          ;  
        digital_voltage_instruction
          : READ DIGITAL VOLTAGE chip_address
          ;  
        digital_current_instruction
          : READ DIGITAL CURRENT chip_address
          ;  
        l1cnt_instruction
          : READ TRIGGER ONE COUNTER chip_address
          ;
        l2cnt_instruction
          : READ TRIGGER TWO COUNTER chip_address
          ;
        sclkcnt_instruction
          : READ SAMPLE CLOCK COUNTER chip_address 
          ;
        dstbcnt_instruction
          : READ DATA STROBE COUNTER chip_address
          ;
        tsmword_instruction
          : READ TEST MODE WORDS chip_address
          | WRITE TEST MODE WORDS bcast_or_chip_address INTEGER
          ;
        usratio_instruction
          : READ UNDER SAMPLE chip_address
          | WRITE UNDER SAMPLE bcast_or_chip_address INTEGER
          ;
        
        csr_instruction
          : READ ZERO   CONFIG chip_address 
          | READ FIRST  CONFIG chip_address 
          | READ SECOND CONFIG chip_address 
          | READ THIRD  CONFIG chip_address 
          | WRITE ZERO   CONFIG bcast_or_chip_address INTEGER
          | WRITE FIRST  CONFIG bcast_or_chip_address INTEGER
          | WRITE SECOND CONFIG bcast_or_chip_address INTEGER
          | WRITE THIRD  CONFIG bcast_or_chip_address INTEGER
          ;
      @endverbatim
        
      @subsection grammar_bc_cmnds BC Commands
      @verbatim
        cntlat_instruction
          : COUNTER LATCH bcast_or_chip_address
          ;
        cntclr_instruction
          : RESET COUNTER bcast_or_chip_address
          ;
        csr1clr_instruction
          : RESET STATUS bcast_or_chip_address
          ;
        alrst_instruction
          : RESET ALTRO bcast_or_chip_address
          ;
        bcrst_instruction
          : RESET  bcast_or_chip_address
          ;
        stcnv_instruction
          : MONITOR bcast_or_chip_address
          ;
        scevl_instruction
          : SCAN EVENT LENGTH bcast_or_chip_address
          ;
        evlrdo_instruction
          : READ EVENT LENGTH chip_address
          ;
        sttsm_instruction
          : TEST MODE bcast_or_chip_address
          ;
        acqrdo_instruction
          : READ EVENT DATA chip_address
          ;
      @endverbatim

      @subsection grammar_fmd_instr FMD instructions 
      @verbatim
        fmd_instruction 
          : bias_instruction
          | vfp_instruction
          | vfs_instruction
          | cal_lvl_instruction
          | shift_clk_instruction
          | sample_clk_instruction
          | hold_delay_instruction
          | l1_timeout_instruction
          | l2_timeout_instruction
          | range_instruction
          | cmd_instruction
          | l0_instruction
          | l1_instruction
          ;
        
        bias_instruction
          : READ BIAS chip_address
          | WRITE BIAS bcast_or_chip_address INTEGER
          | WRITE BIAS bcast_or_chip_address INTEGER INTEGER
          ;
        vfp_instruction
          : READ VFP chip_address
          | WRITE VFP bcast_or_chip_address INTEGER
          | WRITE VFP bcast_or_chip_address INTEGER INTEGER
          ;
        vfs_instruction
          : READ VFS chip_address
          | WRITE VFS bcast_or_chip_address INTEGER
          | WRITE VFS bcast_or_chip_address INTEGER INTEGER
          ;
        cal_lvl_instruction
          : READ PULSER chip_address
          | WRITE PULSER bcast_or_chip_address INTEGER
          ;
        shift_clk_instruction
          : READ SHIFT CLOCK chip_address
          | WRITE SHIFT CLOCK bcast_or_chip_address INTEGER
          | WRITE SHIFT CLOCK bcast_or_chip_address INTEGER INTEGER
          ;
        sample_clk_instruction
          : READ SAMPLE CLOCK chip_address
          | WRITE SAMPLE CLOCK bcast_or_chip_address INTEGER
          | WRITE SAMPLE CLOCK bcast_or_chip_address INTEGER INTEGER
          ;
        hold_delay_instruction
          : READ HOLD DELAY chip_address
          | WRITE HOLD DELAY bcast_or_chip_address INTEGER
          ;
        l1_timeout_instruction
          : READ TRIGGER ONE TIMEOUT chip_address
          | WRITE TRIGGER ONE TIMEOUT bcast_or_chip_address INTEGER
          ;
        l2_timeout_instruction
          : READ TRIGGER TWO TIMEOUT chip_address
          | WRITE TRIGGER TWO TIMEOUT bcast_or_chip_address INTEGER
          ;
        range_instruction
          : READ RANGE chip_address
          | WRITE RANGE bcast_or_chip_address INTEGER INTEGER
          ;
        cmd_instruction
          : fmd_cmd bcast_or_chip_address
          ;
        
        fmd_cmd
          : CHANGE VOLTAGE
          | CHANGE TIMEOUT
          | CHANGE RANGE
          | CHANGE CLOCK
          | PULSER MODE ON
          | PULSER MODE OFF
          ;
        
        l0_instruction
          : READ TRIGGER ZERO chip_address
          ;
        l1_instruction
          : READ TRIGGER ONE chip_address
          ;
      @endverbatim       
*/
  
/*
 * EOF
 */


