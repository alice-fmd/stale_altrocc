//
//
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//   This library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public License 
//   as published by the Free Software Foundation; either version 2 of 
//   the License, or (at your option) any later version.  
//
//   This library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details. 
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with this library; if not, write to the Free
//   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
//   MA 02111-1307  USA  
//
//
/** @file   doc/examples.c
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Examples. */


/** @page examples Examples 
    Here are the various examples.
 */
/** @example altro_1st_bl.rcu
    @par altro_1st_bl example */
/** @example altro_1st_data_path.rcu
    @par altro_1st_data_path example */
/** @example altro_2nd_bl.rcu
    @par altro_2nd_bl example */
/** @example altro_2nd_data_path.rcu
    @par altro_2nd_data_path example */
/** @example altro_ch_pedestal.rcu
    @par altro_ch_pedestal example */
/** @example altro_data_path.rcu
    @par altro_data_path example */
/** @example altro_k.rcu
    @par altro_k example */
/** @example altro_misc.rcu
    @par altro_misc example */
/** @example altro_pedestal_address.rcu
    @par altro_pedestal_address example */
/** @example altro_tr_cfg.rcu
    @par altro_tr_cfg example */
/** @example altro_zero_sup.rcu
    @par altro_zero_sup example */
/** @example jump.rcu
    @par jump example */
/** @example loop.rcu
    @par loop example */
/** @example rcu_pedestal.rcu
    @par rcu_pedestal example */
/** @example rcu_readout.rcu
    @par rcu_readout example */
/** @example rcu_reset.rcu
    @par rcu_reset example */
/** @example rcu_trigger.rcu
    @par rcu_trigger example */
/** @example rcu_wait.rcu
    @par rcu_wait example */
/** @example bc_analog.rcu
    @par bc_analog.rcu example */
/** @example bc_cmds.rcu
    @par bc_cmds.rcu example */
/** @example bc_counters.rcu
    @par bc_counters.rcu example */
/** @example bc_csr.rcu
    @par bc_csr.rcu example */
/** @example bc_digital.rcu
    @par bc_digital.rcu example */
/** @example bc_temp.rcu
    @par bc_temp.rcu example */
/** @example bc_testmode.rcu
    @par bc_testmode.rcu example */
/** @example bc_voltage.rcu
    @par bc_voltage.rcu example */

#error Not for compilation 
/*
 * EOF
 */
