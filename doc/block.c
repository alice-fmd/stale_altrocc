/*
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   block.c
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Blocks. */
/** @page block Blocks

    A valid RCU input file consists of zero or more @e groups of
    instructions.   A group consists of the keyword @c GROUP followed
    by a list of instructions, and ended by the keyword @c END 

    @verbatim 
    GROUP 
      # List of instructions follows 
    END 
    @endverbatim

*/
/*
 * EOF
 */
