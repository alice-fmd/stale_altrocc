/*
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   rcu.c
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  RCU Instructions. */
/** @page rcu RCU Instructions.

    RCU instructions are preceeded by the keyword @c RCU.  

    The RCU instructions are 

    @par RESET STATUS (@c RS_STATUS)
    Reset the status register 

    @par RESET TRIGGER CONFIG (@c RS_TRCFG)
    Reset the trigger configuration 

    @par RESET TRIGGER COUNTER (@c RS_TRCNT)
    Reset the trigger counter 

    @par READOUT (@c CHRDO) 
    Readout all active channels 

    @par PEDESTAL READ @a address  (@c PMREAD)
    Read the pattern memory into the pedestal memories of the ALTRO
    chips.  The ALTRO channels that are written to is determined by @a
    addr.  If the second argument is non-zero, then the pattern memory
    is broadcast over all the ALTRO channels

    @par PEDESTAL WRITE @< BROADCAST | @a channel @> (@c PMWRITE)
    Read the pattern memory into the pedestal memories of the ALTRO
    chips.  The ALTRO channels that are written to is determined by @a
    addr.  If the second argument is non-zero, then the pattern memory
    is broadcast over all the ALTRO channels

    @par WAIT @a delay (@c WAIT)
    Wait of @a delay number of clock cycles at the current
    instruction.

    @par TRIGGER (@c TRIGGER) 
    Make a hardware trigger (L1), which will be treated as an external
    trigger

*/
/*
 * EOF
 */
