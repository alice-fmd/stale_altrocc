/*
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   altro.c
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  ALTRO Instructions. */
/** @page altro ALTRO Instructions.

    ALTRO instructions are preceeded by the keyword @c ALTRO. 

    All ALTRO @c READ instructions read the data from the ALTRO's into
    the RCU result memory.   

    The ALTRO instructions are 

    @par READ @< K1 | K2 | K3 @> @< @a channel_address | BROADCAST @> (@c K1)
    Read the parameters K1, K2, or K3 of @a channel_address or all
    ALTRO's. 

    @par WRITE @< K1 | K2 | K3 @> @< @a channel_address | BROADCAST @> @a value
    Write the value of parameters K1, K2, or K3 to the channel @a
    channel_address or all ALTRO's. 

    @par READ @< L1 | L2 | L3 @> @< @a channel_address | BROADCAST @> (@c L1)
    Read the parameters L1, L2, or L3  of @a channel_address or all
    ALTRO's. 

    @par WRITE @< L1 | L2 | L3 @> @< @a channel_address | BROADCAST @> @a value
    Write the value of parameters L1, L2, or L3 to the channel @a
    channel_address or all ALTRO's. 

    @par READ FIRST BASELINE @< @a channel_address | BROADCAST @> (@c VFPED)
    Read the variable and fixed pedestals of @a channel_address or all
    ALTROs. 

    @par WRITE FIRST BASELINE @< @a channel_address | BROADCAST @> @a value (@c VFPED)
    Write the fixed pedestals @a value to @a channel_address or all
    ALTROs. 

    @par READ PEDESTAL @< @a channel_address | BROADCAST @> (@c PMDTA)
    Read the address of one or all ALTRO chips where they will fetch
    the pedestal values from.

    @par WRITE PEDESTAL @< @a channel_address | BROADCAST @> @a value
    Write the address to one or all ALTRO chip(s) where it should read
    the pedestals from.

    @par READ ZERO SUPPRESSION @< @a chip_address | BROADCAST @> (@c ZSTHR)
    Read the zero suppression register of one or all ALTRO chips into
    the result memory of the RCU.

    @par WRITE ZERO SUPPRESSION @< @a chip_address | BROADCAST @> @a off @a thr
    Write the zero suppression registers into one or all ALTRO chips. 
    @a off is the offset to be added to the signal, and @a thr is the
    zero suppression threshold.  

    @par READ SECOND BASELINE @< @a chip_address | BROADCAST @> (@c BCTHR)
    Read the second baseline correction registers of one or all ALTRO
    chips into the RCU result memory.

    @par WRITE SECOND BASELINE @< @a chip_address | BROADCAST @> @a low @a high
    Write the second base line configuration into one or all ALTRO
    chips.  @a high is the high threshold, and @a low is low
    threshold. 

    @par READ TRIGGER CONFIG @< @a chip_address | BROADCAST @> (@c TRCFG)
    Read the trigger configuration of one or all ALTRO chips into the
    RCU result memory.

    @par WRITE TRIGGER CONFIG @< @a chip_address | BROADCAST @> @a start @a end
    Write the trigger configuration into one or all ALTRO chips.  @a
    start is which time step to start sampling at, and @a end is which
    time step to stop sampling at

    @par READ FIRST PATH @< @a chip_address | BROADCAST @> (@c DPCFG)
    Read the data path configuration of one or all ALTRO chips into
    the RCU result memory

    @par WRITE FIRST PATH @< @a chip_address | BROADCAST @> [INVERT] [FIRST BASELINE @a mode] [SECOND BASELINE @a pre @a post] [ZERO SUPPRESSION @a conf @a postz @a prez]
    Write the data path configuration into one or all ALTRO chips.  If
    @c INVERT is given then, invert the bits of the read-out data.  @a
    mode is the mode of the first baseline correction, @a pre is the
    number of pre-samples excluded from the second baseline
    correction, @a post the number of post-samples excluded from the
    second baseline correction, @a conf is the Glitch filter
    configuration of the zero suppression, @a postz is the number of
    post-samples excluded from the zero suppression, and @a prez is
    the number of post-samples excluded from the zero suppression.


    @par READ SECOND PATH @< @a chip_address | BROADCAST @> (@c BFNPT) 
    Read the second data path configuration of one or all ALTRO chips
    into the result memory of the RCU.

    @par WRITE SECOND PATH @< @a chip_address | BROADCAST @> [PRE TRIGGER @a n] [BUFFER SIZE @a size] [FILTER] [POWER SAVE]
    Write the second data path configuration into one or all ALTRO
    chips. @a n is how many samples to take before the trigger, @a
    size is the buffer size (zero=4, and one=8). If @c FILTER is
    given, then enable digital filter.  If @c POWER @c SAVE is given
    then enable powersave 


    @par READ PEDESTAL ADDRESS @< @a chip_address | BROADCAST @> (@c PMADD)
    Read the address where pedestal data is at into the RCU result memory

    @par WRITE PEDESTAL ADDRESS @< @a chip_address | BROADCAST @> @a value
    Write the address of the pedestal memory to write across the
    pedestals. @a value is the address where the pedestal memory
    should be read from.

    @par READ ERROR @a chip_address (@c ERSTR)
    Read the error register of one ALTRO chip into the result memory
    of the RCU

    @par READ ADDRESS @a chip_address @a channel_address (@c ADEVL)
    Read the address and event length of one ALTRO channel into the
    RCU result memory.

    @par READ TRIGGER COUNTER  @< @a chip_address | BROADCAST @> (@c TRCNT)
    Read the trigger counter register of one or all ALTRO chips into
    the RCU result memory

    @par WRITE INCREMENT @< @a chip_address | BROADCAST @> (@c WPINC)
    Increment the write pointer of one or all ALTRO chips.  This is
    equivilant to a Level 2 trigger

    @par READ INCREMENT @< @a chip_address | BROADCAST @> (@c RPINC)
    Increment the read pointer of one or all ALTRO chips.  This
    releases one data buffer in the ALTRO's

    @par READOUT @a channel_address @a chip_address (@c CHRDO)
    Read out channel @a channel_address of ALTRO chip @a chip_address

    @par SOFTWARE TRIGGER @< @a chip_address | BROADCAST @> (@c SWTRG)
    Make a software trigger.

    @par RESET TRIGGER COUNTER @< @a chip_address | BROADCAST @> (@c TRCLR)
    Clear the trigger counter of one or all ALTRO chips.

    @par RESET ERROR @< @a chip_address | BROADCAST @> (@c ERCLR)
    Clear the error register of one or all ALTRO chips.

*/
/*
 * EOF
 */
