/*
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   instr.c
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Instructions. */
/** @page instr Instructions

    Instructions are recognised by a leading string.  Valid leading
    strings are @c JUMP, @c LOOP, @c RCU, and @c ALTRO.  

    @verbatim 
    GROUP 
      LOOP ... 
      JUMP ... 
      RCU ... 
      ALTRO ... 
    END 
    @endverbatim

    @par Labels 
    
    Any instruction can be preceeded by a label.   A label is an
    identifier followed by a literal colon (@c :). 

    @verbatim 
    GROUP 
             LOOP ... 
             JUMP ... 
      label: RCU ... 
             ALTRO ... 
    END 
    @endverbatim

    It's stronly recommended to put a label on the first instruction
    in a group, so one can easily execute that instruction from
    outside the RCU. 

    @verbatim 
    GROUP my_function : 
      # Instructions follows
      ... 
    END 
    @endverbatim

    - @subpage special
    - @subpage rcu
    - @subpage bc
    - @subpage fmd
    - @subpage altro
*/
/*
 * EOF
 */
