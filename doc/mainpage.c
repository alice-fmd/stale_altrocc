/*
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   mainpage.c
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Title page documentation. */
/** @mainpage Compiler for the ALICE RCU 

    @section intro Introduction. 
    
    This library contains a compiler for the RCU and ALTRO
    instructions.   The user will write file in a human readable
    format, which the compiler then translates to the binary code one
    can  upload into the RCU instruction memory and subsequently
    execute.  

    For example, the input 
    @verbatim 
    group altro_tr_cfg 
      ALTRO write trigger config broadcast 0 0x64
    end
    @endverbatim 

    can be passed to the compiler  
    @verbatim 
    rcu altro_tr_cfg.rcu 
    @endverbatim 

    which will produce the output 
    @verbatim 
      0      altro_tr_cfg 0x0064000a
      1                   0x00700064
      2                   0x00390000
    @endverbatim 

    - @subpage lang
      - @subpage lexi
      - @subpage com
      - @subpage block
      - @subpage instr
        - @subpage special
        - @subpage rcu
        - @subpage bc
        - @subpage fmd
        - @subpage altro
    - @subpage app_grammar
*/

#error Not for compilation
//
// EOF
//
