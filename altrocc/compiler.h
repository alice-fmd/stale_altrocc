/* 
 *
 * Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307 USA
 */
#ifndef RCUC_COMPILER_H
#define RCUC_COMPILER_H
/** @file    compiler.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jan 26 14:43:34 2005
    @brief   Compiler declarations
    
*/
/** @defgroup compiler Compiler code
    @brief Compiler of instruction sets for the ALICE RCU 
*/

#include <stdio.h>
#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

/* ________________________________________________________________ */
#define YYSTYPE u_int

  /** @struct RCUC_set 
    @brief The set of compiled instructions 
    @ingroup compiler 
 */
  struct RCUC_set 
  {
    /** The number of instructions in the set */
    u_int   size;
    /** The number of allocated slots in the set. */
    u_int   alloc;
    /** The instruction array of the set */
    u_int*  data;
    /** The instruction array of the set */
    char**  labels;
  };


  /** @defgroup scanner Scanner interface */
  /** Set the current label.  This is called by the lexical analyser to
      set the next label 
      @ingroup scanner 
      @param lab The label text 
      @param len The length of @a len */
  void  RCUC_set_label(char* lab, int len);
  /** Increment the current line number 
      @ingroup scanner 
  */
  void  RCUC_lineno_increment();

  /* ________________________________________________________________ */
  /** @defgroup parser Parse tree interface */
  /** Add an instruction to the instruction set.  This is called by the
      parser to add an instruction to the instruction set. 
      @ingroup parser 
      @param i The instruction
      @param lab If non-zero, this instruction will have a label
      @return the instruction.  */
  u_int RCUC_add_instruction(u_int i, u_int lab);

  /* Reference resolution */
  /** Resolve an offset given to a @c LOOP or @c JUMP instruction.  This
      is called from the parser to turn relative off sets into absolute
      addresses 
      @ingroup parser 
      @param off The address of set 
      @return The absolute address pointed to by @a off  */
  u_int RCUC_OFFSET_resolve(int off);
  /** Get the absolute address of the label refered to by the @c LOOP or
      @c JUMP instruction.  Note, that the label @e must already be
      defined.  This makes sense, as there's no branching constructions
      in the RCU language, so it makes little sense to jump to a later
      instrustion.   Note, that the label is set by the lexical
      analyser, so no arguments need be passed to this function. 
      @ingroup parser 
      @return  The absolute address of the label. */
  u_int RCUC_LABEL_resolve();

  /* ________________________________________________________________ */
  /** @defgroup rcu RCU instructions */
  /** Encode an RCU instruction.  In essence, this function adds 
      @c (1<<22)  to @a instr
      @ingroup rcu 
      @param instr The instruction to encode as an RCU instruction. 
      @return The RCU instruction, ready to be put into the compiled
      instruction set.  */
  u_int RCUC_RCU_encode(u_int instr);
  /** Encode a loop instruction
      @ingroup rcu 
      @param addr Aboslute address jump to 
      @param n Number of times to perform the loop
      @return The instruction  */
  u_int RCUC_LOOP_encode(u_int addr, u_int n);
  /** Encode a @c JUMP instruction 
      @ingroup rcu 
      @param addr Absolute address to jump to  
      @return  The instruction */
  u_int RCUC_JUMP_encode(u_int addr);
  /** Encode a @c RS_STATUS instruction.  That is, reset the status
      register of the RCU 
      @ingroup rcu 
      @return The partially compiled instruction */
  u_int RCUC_RCU_RS_STATUS_encode();
  /** Encode a @c RS_TRCFG instruction. That is, reset the trigger
      config register of the RCU 
      @ingroup rcu 
      @return The partially compiled instruction */
  u_int RCUC_RCU_RS_TRCFG_encode();
  /** Encode a @c RS_TRCNT instruction. That is, reset the trigger
      counter register of the RCU 
      @ingroup rcu 
      @return The partially compiled instruction */
  u_int RCUC_RCU_RS_TRCNT_encode();
  /** Encode a @c CHRDO instruction.  That is, perform the read out of
      all active channels. 
      @ingroup rcu 
      @return The partially compiled instruction */
  u_int RCUC_RCU_CHRDO_encode();
  /** Encode a @c PMREAD instruction.  Read pedestals from address @a
      addr into pattern memory off the RCU.  
      @ingroup rcu 
      @param addr The channel to read from 
      @return The partially compiled instruction */
  u_int RCUC_RCU_PMREAD_encode(u_int addr);
  /** Encode a @c PMWRITE instruction.  Read the pattern memory into the
      pedestal memories of the ALTRO chips.  The ALTRO channels that are
      written to is determined by @a addr.   If the second argument is
      non-zero, then the pattern memory is broadcast over all the ALTRO
      channels 
      @ingroup rcu 
      @param addr Address to write pattern memory to, or 
      @param broadcast if non-zero, broadcast to all ALTRO channels 
      @return The partially compiled instruction */
  u_int RCUC_RCU_PMWRITE_encode(u_int addr, u_int broadcast);
  /** Encode a @c END instruction.  That is, the end of one instruction
      set.  This is automatically added after each @c GROUP in a valid
      input file. 
      @ingroup rcu 
      @return The partially compiled instruction */
  u_int RCUC_END_encode();
  /** Encode a @c WAIT instruction.  Wait of @a delay number of clock
      cycles at the current instruction. 
      @ingroup rcu 
      @param delay Number of clock cycles to wait for. 
      @return The partially compiled instruction */
  u_int RCUC_RCU_WAIT_encode(u_int delay);
  /** Encode a @c TRIGGER instruction.  Make a hardware trigger (L1),
      which will be treated as an external trigger 
      @ingroup rcu 
      @return The partially compiled instruction */
  u_int RCUC_RCU_TRIGGER_encode();

  /* ________________________________________________________________ */
  /** @defgroup altro ALTRO instructions */
  /** Encode an ALTRO instruction.  This really puts @e two
      instructions into the instruction set: one for the instruction
      itself, and one for the data.  
      @ingroup altro 
      @param instr The instruction to encode.  The data is set by the
      other ALTRO encoding functions, and need not be passed here. 
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_encode(u_int instr);
  /** Encode an ALTRO @c BROADCAST instruction.  Make the instruction
      be a broadcast to all ALTRO channels.  
      @ingroup altro 
      @return The partially compiled instruction */
  u_int RCUC_BROADCAST_encode();
  /** Encode an ALTRO @c LOCAL instruction.  Encode a channel local
      addres. 
      @ingroup altro 
      @param board Board address
      @param chip  Chip addres
      @param ch    Channel address. 
      @return The partially compiled instruction */
  u_int RCUC_BOARD_CHIP_CHANNEL_encode(u_int board, u_int chip, u_int ch);
  /** Encode an ALTRO @c BOARD instruction. Encode a board address. 
      @ingroup altro 
      @param board The board address. 
      @return The partially compiled instruction */
  u_int RCUC_BOARD_encode(u_int board);
  /** Encode an ALTRO @c BOARD,CHIP instruction. Encode a board,chip address. 
      @ingroup altro 
      @param board The board address. 
      @param chip The chip address. 
      @return The partially compiled instruction */
  u_int RCUC_BOARD_CHIP_encode(u_int board, u_int chip);
  /** Encode an ALTRO @c CHIP instruction. Encode a chip address. 
      @ingroup altro 
      @param chip The chip address. 
      @return The partially compiled instruction */
  u_int RCUC_CHIP_encode(u_int chip);
  /** Encode a channel address
      @ingroup altro
      @param ch Channel number 
      @return The partially compiled instruction */
  u_int RCUC_CHANNEL_encode(u_int ch);

  /** Encode an ALTRO @c K_read instruction.  Encode an instruction to
      read the K1, K2, and K3 registers of one or all ALTRO chips.
      The result of the read is put in the result memory of the RCU
      chip. 
      @ingroup altro 
      @param which Which of the K parameters to read (1,2, or 3)
      @param address Which channel to read from (possibly all)
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_K_read_encode(u_short which, u_int address);
  /** Encode an ALTRO @c K_write instruction.  Encode an instruction to
      write the K1, K2, and K3 registers of one or all ALTRO chips.
      @ingroup altro 
      @param which Which of the K parameters to write (1,2, or 3)
      @param address Which channel to write to (possibly all)
      @param data The value of the K parameter 
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_K_write_encode(u_short which, u_int address, u_int data);
  /** Encode an ALTRO @c L_read instruction.  Encode an instruction to
      read the L1, L2, and L3 registers of one or all ALTRO chips.
      The result of the read is put in the result memory of the RCU
      chip. 
      @ingroup altro 
      @param which Which of the K parameters to read (1,2, or 3)
      @param address Which channel to read from (possibly all)
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_L_read_encode(u_short which, u_int address);
  /** Encode an ALTRO @c L_write instruction.  Encode an instruction to
      write the L1, L2, and L3 registers of one or all ALTRO chips.
      @ingroup altro 
      @param which Which of the K parameters to write (1,2, or 3)
      @param address Which channel to write to (possibly all)
      @param data The value of the K parameter 
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_L_write_encode(u_short which, u_int address, u_int data);
  /** Encode an ALTRO @c VFPED_read instruction.  Read the variable and
      fixed pedestal register(s) of one or all ALTRO into the result
      memory of the RCU.  
      @ingroup altro 
      @param addr ALTRO address to read from (possibly all)
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_VFPED_read_encode(u_int addr);
  /** Encode an ALTRO @c VFPED_write instruction.  Write the fixed
      pedestal into the ALTRO memory of one or all chips. 
      @ingroup altro 
      @param addr ALTRO chip address (possibly all)
      @param data The value of the fixed pedestal. 
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_VFPED_write_encode(u_int addr, u_int data);
  /** Encode an ALTRO @c PMDTA_read instruction.   Read the address of
      one or all ALTRO chips where they will fetch the pedestal values
      from. 
      @ingroup altro 
      @param addr Address of the chip (possibly all)
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_PMDTA_read_encode(u_int addr);
  /** Encode an ALTRO @c PMDTA_write instruction. Write the address to
      one or all ALTRO chip(s) where it should read the pedestals
      from. 
      @ingroup altro 
      @param addr Address of the ALTRO channel (possibly all)
      @param data The address to read the pedestals from. 
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_PMDTA_write_encode(u_int addr, u_int data);
  /** Encode an ALTRO @c ZSTHR_read instruction.  Read the zero
      suppression register of one or all ALTRO chips into the result
      memory of the RCU. 
      @ingroup altro 
      @param addr ALTRO chip to read from (possibly all)
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_ZSTHR_read_encode(u_int addr);
  /** Encode an ALTRO @c ZSTHR_write instruction.  Write the zero
      suppression registers into one or all ALTRO chips. 
      @ingroup altro 
      @param addr The ALTRO chip to write to (possibly all)
      @param off The offset to be added to the signal
      @param thr The zero suppression threshold. 
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_ZSTHR_write_encode(u_int addr, u_int off, u_int thr);
  /** Encode an ALTRO @c BCTHR_read instruction.  Read the second
      baseline correction registers of one or all ALTRO chips into the
      RCU result memory.
      @ingroup altro 
      @param addr The ALTRO chip to read from (possibly all)
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_BCTHR_read_encode(u_int addr);
  /** Encode an ALTRO @c BCTHR_write instruction.  Write the second
      base line configuration into one or all ALTRO chips. 
      @ingroup altro 
      @param addr ALTRO chip to write to
      @param high High threshold
      @param low Low threshold
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_BCTHR_write_encode(u_int addr, u_int high, u_int low);
  /** Encode an ALTRO @c TRCFG_read instruction.  Read the trigger
      configuration of one or all ALTRO chips into the RCU result
      memory.  @ingroup altro @param addr The ALTRO chip to read from
      (possibly all) @return The partially compiled instruction */
  u_int RCUC_ALTRO_TRCFG_read_encode(u_int addr);
  /** Encode an ALTRO @c TRCFG_write instruction.  Write the trigger
      configuration into one or all ALTRO chips. 
      @ingroup altro 
      @param addr The ALTRO chip to write to (possibly all)
      @param start Which time step to start sampling at 
      @param end Which time step to stop sampling at 
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_TRCFG_write_encode(u_int addr, u_int start, u_int end);
  /** Encode an ALTRO @c DPCFG_read instruction.  Read the data path
      configuration of one or all ALTRO chips into the RCU result
      memory 
      @ingroup altro 
      @param addr The ALTRO chip to read from (possibly all)
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_DPCFG_read_encode(u_int addr);
  /** Encode an ALTRO @c DPCFG_write instruction.  Write the data path
      configuration into one or all ALTRO chips. 
      @ingroup altro 
      @param addr The ALTRO chip to write to (possibly all)
      @param invert If non-zero, invert the bits of the read-out data. 
      @param first_bl First baseline mode (see the ALTRO documentation)
      @param second_bl Second baseline paameters (see the ALTRO
      documentation) 
      @param zero_sup Zero suppression parameters. (see the ALTRO
      documentation) 
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_DPCFG_write_encode(u_int addr, u_int invert, u_int first_bl,
				u_int second_bl, u_int zero_sup);
  /** Encode an ALTRO @c FIRST_BASELINE instruction
      @ingroup altro 
      @param mode The first baeline mode.  A bit pattern of 4 bits.  See
      the ALTRO documentation for the various configurations. 
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_FIRST_BASELINE_encode(u_int mode) ;
  /** Encode an ALTRO @c SECOND_BASELINE instruction.  Second baseline
      parameters. 
      @ingroup altro 
      @param pre Number of pre-samples excluded from the second baseline
      correction. 
      @param post Number of post-samples excluded from the second
      baseline correction. 
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_SECOND_BASELINE_encode(u_int pre, u_int post) ;
  /** Encode an ALTRO @c ZERO_SUPPRESSION instruction.  Zero
      suppression parameters
      @ingroup altro 
      @param conf Glitch filter configuration of the zero suppression.
      This is a pattern of 2 bits.  See the ALTRO documentation for the
      various configurations.  
      @param post Number of post-samples excluded from the zero
      suppression. 
      @param pre Number of post-samples excluded from the zero
      suppression. 
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_ZERO_SUPPRESSION_encode(u_int conf, u_int post, u_int pre) ;
  /** Encode an ALTRO @c BFNPT_read instruction.  Read the second data
      path configuration of one or all ALTRO chips into the result
      memory of the RCU. 
      @ingroup altro 
      @param addr The ALTRO chip to read from (possibly all)
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_BFNPT_read_encode(u_int addr);
  /** Encode an ALTRO @c BFNPT_write instruction.    Write the second
      data path configuration into one or all ALTRO chips. 
      @ingroup altro 
      @param addr The ALTRO chip to write to (possibly all)
      @param pretrigger How many samples to take before the trigger. 
      @param bufsize The buffer size (zero=4, and one=8). 
      @param filter Enable digital filter 
      @param powersave Enable powersave 
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_BFNPT_write_encode(u_int addr, u_int pretrigger, u_int bufsize,
				u_int filter, u_int powersave);
  /** Encode an ALTRO @c PMADD_read instruction.  Read the address
      where pedestal data is at into the RCU result memory
      @ingroup altro 
      @param addr The ALTRO chip to read from (possibly all)
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_PMADD_read_encode(u_int addr);
  /** Encode an ALTRO @c PMADD_write instruction.  Write the address of
      the pedestal memory to write across the pedestals. 
      @ingroup altro 
      @param addr The ALTRO chip address (possibly all)
      @param data The address where the pedestal memory should be read
      from. 
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_PMADD_write_encode(u_int addr, u_int data);
  /** Encode an ALTRO @c ERSTR_read instruction.  Read the error
      register of one ALTRO chip into the result memory of the RCU 
      @ingroup altro 
      @param addr The ALTRO chip to read from
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_ERSTR_read_encode(u_int addr);
  /** Encode an ALTRO @c ADEVL_read instruction.  Read the address and
      event length of one ALTRO channel into the RCU result memory. 
      @ingroup altro 
      @param chip The ALTRO chip to read from 
      @param ch The channel of the ALTRO chip to read from 
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_ADEVL_read_encode(u_int chip, u_int ch);
  /** Encode an ALTRO @c TRCNT_read instruction.  Read the trigger
      counter register of one or all ALTRO chips into the RCU result
      memory 
      @ingroup altro 
      @param addr The ALTRO chip to read from (possibly all)
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_TRCNT_read_encode(u_int addr);
  /** Encode an ALTRO @c WPINC_read instruction.  Increment the write
      pointer of one or all ALTRO chips.  This is equivilant to a Level
      2 trigger 
      @ingroup altro 
      @param addr The ALTRO chip to read from (possibly all)
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_WPINC_cmd_encode(u_int addr);
  /** Encode an ALTRO @c RPINC_read instruction.  Increment the read
      pointer of one or all ALTRO chips.  This releases one data buffer
      in the ALTRO's 
      @ingroup altro 
      @param addr The ALTRO chip to read from (possibly all)
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_RPINC_cmd_encode(u_int addr);
  /** Encode an ALTRO @c CHRDO_read instruction
      @ingroup altro 
      @param chip 
      @param ch 
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_CHRDO_cmd_encode(u_int chip, u_int ch);
  /** Encode an ALTRO @c SWTRG_read instruction.  Make a software
      trigger. 
      @ingroup altro 
      @param addr The ALTRO chip to send command to  (possibly all)
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_SWTRG_cmd_encode(u_int addr);
  /** Encode an ALTRO @c TRCLR_read instruction.  Clear the trigger
      counter of one or all ALTRO chips.
      @ingroup altro
      @param addr The ALTRO chip to send command to (possibly all)
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_TRCLR_cmd_encode(u_int addr);
  /** Encode an ALTRO @c ERCLR_read instruction.  Clear the error
      register of one or all ALTRO chips. 
      @ingroup altro 
      @param addr The ALTRO chip to send command to (possibly all)
      @return The partially compiled instruction */
  u_int RCUC_ALTRO_ERCLR_cmd_encode(u_int addr);

  /* ________________________________________________________________ */
  /** @defgroup bc Board-controller instructions */
  /** Encode a BC instruction.  This really puts @e two
      instructions into the instruction set: one for the instruction
      itself, and one for the data.  
      @ingroup bc
      @param instr The instruction to encode.  The data is set by the
      other BC encoding functions, and need not be passed here. 
      @return The partially compiled instruction */
  u_int RCUC_BC_encode(u_int instr);
  /** Encode instruction to read the BC register T_TH
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_T_TH_read_encode(u_int addr);
  /** Encode instruction to write @a data to the BC register @c T_TH
      @ingroup bc
      @param addr Address 
      @param data Value to write to @c T_TH
      @return partially compiled instruction. */
  u_int RCUC_BC_T_TH_write_encode(u_int addr, u_int data);
  /** Encode instruction to read the BC register AV_TH
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_AV_TH_read_encode(u_int addr);
  /** Encode instruction to write @a data to the BC register @c AV_TH
      @ingroup bc
      @param addr Address 
      @param data Value to write to @c AV_TH
      @return partially compiled instruction. */
  u_int RCUC_BC_AV_TH_write_encode(u_int addr, u_int data);

  /** Encode instruction to read the BC register AC_TH
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_AC_TH_read_encode(u_int addr);
  /** Encode instruction to write @a data to the BC register @c AC_TH
      @ingroup bc
      @param addr Address 
      @param data Value to write to @c AC_TH
      @return partially compiled instruction. */
  u_int RCUC_BC_AC_TH_write_encode(u_int addr, u_int data);
  /** Encode instruction to read the BC register DV_TH
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_DV_TH_read_encode(u_int addr);
  /** Encode instruction to write @a data to the BC register @c DV_TH
      @ingroup bc
      @param addr Address 
      @param data Value to write to @c DV_TH
      @return partially compiled instruction. */
  u_int RCUC_BC_DV_TH_write_encode(u_int addr, u_int data);

  /** Encode instruction to read the BC register DC_TH
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_DC_TH_read_encode(u_int addr);
  /** Encode instruction to write @a data to the BC register @c DC_TH
      @ingroup bc
      @param addr Address 
      @param data Value to write to @c DC_TH
      @return partially compiled instruction. */
  u_int RCUC_BC_DC_TH_write_encode(u_int addr, u_int data);
  /** Encode instruction to read the BC register T
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_T_read_encode(u_int addr);
  /** Encode instruction to read the BC register AV
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_AV_read_encode(u_int addr);
  /** Encode instruction to read the BC register AC
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_AC_read_encode(u_int addr);
  /** Encode instruction to read the BC register DV
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_DV_read_encode(u_int addr);
  /** Encode instruction to read the BC register DC
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_DC_read_encode(u_int addr);
  /** Encode instruction to read the BC register L1CNT
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_L1CNT_read_encode(u_int addr);
  /** Encode instruction to read the BC register L2CNT
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_L2CNT_read_encode(u_int addr);
  /** Encode instruction to read the BC register SCLKCNT
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_SCLKCNT_read_encode(u_int addr);
  /** Encode instruction to read the BC register DSTBCNT
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_DSTBCNT_read_encode(u_int addr);
  /** Encode instruction to read the BC register TSMWORD
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_TSMWORD_read_encode(u_int addr);
  /** Encode instruction to write @a data to the BC register @c TSMWORD
      @ingroup bc
      @param addr Address 
      @param data Value to write to @c TSMWORD
      @return partially compiled instruction. */
  u_int RCUC_BC_TSMWORD_write_encode(u_int addr, u_int data);
  /** Encode instruction to read the BC register USRATIO
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_USRATIO_read_encode(u_int addr);
  /** Encode instruction to write @a data to the BC register @c USRATIO
      @ingroup bc
      @param addr Address 
      @param data Value to write to @c USRATIO
      @return partially compiled instruction. */
  u_int RCUC_BC_USRATIO_write_encode(u_int addr, u_int data);
  /** Encode instruction to read the BC register CSR
      @ingroup bc
      @param which Which CSR register to read (0 to 3)
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_CSR_read_encode(u_int which, u_int addr);
  /** Encode instruction to write @a data to the BC register @c CSR
      @ingroup bc
      @param which Which CSR register to write to (0 to 3)
      @param addr Address 
      @param data Value to write to @c CSR
      @return partially compiled instruction. */
  u_int RCUC_BC_CSR_write_encode(u_int which, u_int addr, u_int data);
  /** Encode instruction to send BC command @c CNTLAT
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_CNTLAT_cmd_encode(u_int addr);
  /** Encode instruction to send BC command @c CNTCLR
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_CNTCLR_cmd_encode(u_int addr);
  /** Encode instruction to send BC command @c CSR1CLR
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_CSR1CLR_cmd_encode(u_int addr);
  /** Encode instruction to send BC command @c ALRST
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_ALRST_cmd_encode(u_int addr);
  /** Encode instruction to send BC command @c BCRST
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_BCRST_cmd_encode(u_int addr);
  /** Encode instruction to send BC command @c STCNV
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_STCNV_cmd_encode(u_int addr);
  /** Encode instruction to send BC command @c SCEVL
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_SCEVL_cmd_encode(u_int addr);
  /** Encode instruction to send BC command @c EVLRDO
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_EVLRDO_cmd_encode(u_int addr);
  /** Encode instruction to send BC command @c STTSM
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_STTSM_cmd_encode(u_int addr);
  /** Encode instruction to send BC command @c ACQRDO
      @ingroup bc
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_BC_ACQRDO_cmd_encode(u_int addr);

  /* ________________________________________________________________ */
  /** @defgroup fmd Forward Multiplicity Detector instructions */
  /** Encode an FMD instruction.  This really puts @e two
      instructions into the instruction set: one for the instruction
      itself, and one for the data.  
      @ingroup fmd
      @param instr The instruction to encode.  The data is set by the
      other FMD encoding functions, and need not be passed here. 
      @return The partially compiled instruction */
  u_int RCUC_FMD_encode(u_int instr);
  /** Encode instruction to read the FMD register @c BIAS
      @ingroup fmd
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_FMD_BIAS_read_encode(u_int addr);
  /** Encode instruction to write @a data1 to the FMD register @c BIAS
      @ingroup fmd
      @param addr  Address 
      @param data0 Value to write to @c BIAS
      @param data1 Value to write to @c BIAS
      @return partially compiled instruction. */
  u_int RCUC_FMD_BIAS_write_encode(u_int addr, u_int data0, u_int data1);
  /** Encode instruction to read the FMD register @c VFP
      @ingroup fmd
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_FMD_VFP_read_encode(u_int addr);
  /** Encode instruction to write @a data1 to the FMD register @c VFP
      @ingroup fmd
      @param addr  Address
      @param data0 Value to write to @c VFP
      @param data1 Value to write to @c VFP
      @return partially compiled instruction. */
  u_int RCUC_FMD_VFP_write_encode(u_int addr, u_int data0, u_int data1);
  /** Encode instruction to read the FMD register @c VFS
      @ingroup fmd
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_FMD_VFS_read_encode(u_int addr);
  /** Encode instruction to write @a data1 to the FMD register @c VFS
      @ingroup fmd
      @param addr  Address 
      @param data0 Value to write to @c VFS
      @param data1 Value to write to @c VFS
      @return partially compiled instruction. */
  u_int RCUC_FMD_VFS_write_encode(u_int addr, u_int data0, u_int data1);
  /** Encode instruction to read the FMD register @c CAL_LVL
      @ingroup fmd
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_FMD_CAL_LVL_read_encode(u_int addr);
  /** Encode instruction to write @a data1 to the FMD register @c CAL_LVL
      @ingroup fmd
      @param addr  Address 
      @param data0 Value to write to @c CAL_LVL
      @param data1 Value to write to @c CAL_LVL
      @return partially compiled instruction. */
  u_int RCUC_FMD_CAL_LVL_write_encode(u_int addr, u_int data0, u_int data1);
  /** Encode instruction to read the FMD register @c SHIFT_CLK
      @ingroup fmd
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_FMD_SHIFT_CLK_read_encode(u_int addr);
  /** Encode instruction to write @a data1 to the FMD register @c SHIFT_CLK
      @ingroup fmd
      @param addr  Address 
      @param data0 Value to write to @c SHIFT_CLK
      @param data1 Value to write to @c SHIFT_CLK
      @return partially compiled instruction. */
  u_int RCUC_FMD_SHIFT_CLK_write_encode(u_int addr, u_int data0, u_int data1);
  /** Encode instruction to read the FMD register @c SAMPLE_CLK
      @ingroup fmd
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_FMD_SAMPLE_CLK_read_encode(u_int addr);
  /** Encode instruction to write @a data1 to the FMD register @c SAMPLE_CLK
      @ingroup fmd
      @param addr  Address 
      @param data0 Value to write to @c SAMPLE_CLK
      @param data1 Value to write to @c SAMPLE_CLK
      @return partially compiled instruction. */
  u_int RCUC_FMD_SAMPLE_CLK_write_encode(u_int addr, u_int data0, u_int data1);
  /** Encode instruction to read the FMD register @c HOLD_DELAY
      @ingroup fmd
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_FMD_HOLD_DELAY_read_encode(u_int addr);
  /** Encode instruction to write @a data0 to the FMD register @c HOLD_DELAY
      @ingroup fmd
      @param addr Address 
      @param data0 Value to write to @c HOLD_DELAY
      @return partially compiled instruction. */
  u_int RCUC_FMD_HOLD_DELAY_write_encode(u_int addr, u_int data0);
  /** Encode instruction to read the FMD register @c L1_TIMEOUT
      @ingroup fmd
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_FMD_L1_TIMEOUT_read_encode(u_int addr);
  /** Encode instruction to write @a data0 to the FMD register @c L1_TIMEOUT
      @ingroup fmd
      @param addr Address 
      @param data0 Value to write to @c L1_TIMEOUT
      @return partially compiled instruction. */
  u_int RCUC_FMD_L1_TIMEOUT_write_encode(u_int addr, u_int data0);
  /** Encode instruction to read the FMD register @c L2_TIMEOUT
      @ingroup fmd
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_FMD_L2_TIMEOUT_read_encode(u_int addr);
  /** Encode instruction to write @a data0 to the FMD register @c L2_TIMEOUT
      @ingroup fmd
      @param addr Address 
      @param data0 Value to write to @c L2_TIMEOUT
      @return partially compiled instruction. */
  u_int RCUC_FMD_L2_TIMEOUT_write_encode(u_int addr, u_int data0);
  /** Encode instruction to read the FMD register @c RANGE
      @ingroup fmd
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_FMD_RANGE_read_encode(u_int addr);
  /** Encode instruction to write @a data1 to the FMD register @c RANGE
      @ingroup fmd
      @param addr  Address 
      @param data0 Value to write to @c RANGE
      @param data1 Value to write to @c RANGE
      @return partially compiled instruction. */
  u_int RCUC_FMD_RANGE_write_encode(u_int addr, u_int data0, u_int data1);
  /** Encode instruction to send FMD command @c fmd
      @ingroup fmd
      @param cmd  Command number
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_FMD_cmd_encode(u_int cmd, u_int addr);
  /** Encode instruction to send FMD command @c CHG_DAC
      @ingroup fmd
      @return partially compiled instruction. */
  u_int RCUC_FMD_CHG_DAC_cmd_encode();
  /** Encode instruction to send FMD command @c FMD_TRIGGER
      @ingroup fmd
      @return partially compiled instruction. */
  u_int RCUC_FMD_TRIGGER_cmd_encode();
  /** Encode instruction to send FMD command @c PULSER_MODE
      @ingroup fmd
      @param enable Address 
      @return partially compiled instruction. */
  u_int RCUC_FMD_PULSER_MODE_cmd_encode(u_int enable);
  /** Encode instruction to read the FMD register @c L0
      @ingroup fmd
      @param addr Address 
      @return partially compiled instruction. */
  u_int RCUC_FMD_L0_read_encode(u_int addr);
  

  /* ________________________________________________________________ */
  /* Compiler interface */
  /** Compile the file named by @a filename, and return the compiled
      instruction set. 
      @ingroup compiler 
      @param filename File to compile 
      @param trace whether to enable traces
      @param output Output errors and messages to stderr
      @return Compiled instruction set, or NULL on error */
  struct RCUC_set* RCUC_compile(const char* filename, int trace, int output);
  void RCUC_print(const struct RCUC_set* i, FILE* file, short nums, 
		  short labs);
  /** Free the compiled instruction set 
      @ingroup compiler 
      @param i Instruction set to de-allocate */
  void RCUC_free(struct RCUC_set* i);

  /** Push a file on-to the stack of files to process 
      @ingroup compiler
      @param filename File name of file to push.
      @return  */
  u_int RCUC_push_file(const char* filename);
  /** Pop the most recently pushed file on the stack of file to
      compile. 
      @ingroup compiler 
      @return  */
  u_int RCUC_pop_file();
  /** Flag a compilation error 
      @ingroup compiler 
      @param format The message format string 
      @return Not used. */
  u_int RCUC_error(const char* format, ...);
  const char* RCUC_get_error();

#ifdef __cplusplus
}
#endif
#endif
/*
 * EOF
 */
