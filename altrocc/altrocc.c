/* 
 *
 * Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307 USA
 */
/** @file    altrocc.c
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jan 26 14:44:04 2005
    @brief   Compiler main program
    
*/
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <altrocc/compiler.h>

int 
main(int argc, char** argv) 
{
  int i, nums=0, labs=0, trace=0;
  char* input = 0; 
  FILE* ofile=stdout;
  for (i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'h':
	printf("Usage: %s [OPTIONS] FILE\n\n"
	       "Options:\n"
	       "  -h\t\tShow this help\n"
	       "  -l\t\tShow labels\n"
	       "  -n\t\tShow addresses\n"
	       "  -d\t\tShow trace of parser\n"
	       "  -o FILE\tWrite to output file FILE\n\n", 
	       argv[0]);
	return 0;
      case 'l': labs=1 ; break;
      case 'n': nums=1 ; break;
      case 'd': trace=1 ; break;
      case 'o': 
	ofile = fopen(argv[i+1], "w");
	if (!ofile) {
	  fprintf(stderr, "Error: %s\n", strerror(errno));
	  return 1;
	}
	i++;
	break;
      case '\0': input = argv[i]; break;
      default:
	fprintf(stderr, "Unknown option `%s', try `%s -h'\n", 
		argv[i], argv[0]);
	return 1;
      }
    }
    else {
      input = argv[i];
    }
  }
  struct RCUC_set* prog = RCUC_compile(input, trace, 0);
  if (!prog) {
    fprintf(stderr, "%s\n", RCUC_get_error());
    return 1;
  }
  RCUC_print(prog, ofile, nums, labs);
  RCUC_free(prog);
  return 0;

}

