/* 
 *
 * Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307 USA
 */
/** @file    compiler.c
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Jan 26 14:43:44 2005
    @brief   Compiler definitions
    
*/
#include <altrocc/compiler.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#define LABEL_SIZE 256

/* ================================================================= */
extern FILE* yyin;
extern int   yydebug;
extern int   yyparse();

/* ================================================================= */
static struct RCUC_set instructions = { 0, 0, 0 };
static int RCUC_use_stderr = 0;
static char RCUC_last_error[1024];

/** @var current_label 
    @brief Internal cache variable used by the lexical analyser to set
    the next label. 
 */
char current_label[LABEL_SIZE];

/* _________________________________________________________________ */
void  
RCUC_set_label(char* lab, int len) 
{
  int i;
  for (i = 0; i < len; i++) current_label[i] = lab[i];
  current_label[len] = '\0';
}
  
/* _________________________________________________________________ */
static long
RCUC_find_label(const char* name) 
{
  long i = 0;
  for (i = 0; i < instructions.size; i++) {
    if (!instructions.labels[i]) continue;
    if (!strncmp(instructions.labels[i], name, LABEL_SIZE)) return i;
  }
  return -1;
}

/* _________________________________________________________________ */
static const char*
RCUC_find_label_by_address(u_int addr) 
{
  if (addr >= instructions.size) return 0;
  return instructions.labels[addr];
}

/* ================================================================= */
u_int
RCUC_add_instruction(u_int instr, u_int lab)
{
  int n;
  if (!instructions.data) {
    instructions.alloc   = 128;
    instructions.size    = 0;
    instructions.data    = (u_int*)malloc(sizeof(u_int)*instructions.alloc);
    instructions.labels  = (char**)malloc(sizeof(char*)*instructions.alloc);
  }
  if (instructions.size == instructions.alloc) {
    instructions.alloc  *= 1.5 + 1;
    instructions.data   =  (u_int*)realloc(instructions.data, 
					   sizeof(u_int)*instructions.alloc);
    instructions.labels =  (char**)realloc(instructions.labels,
					   sizeof(char*)*instructions.alloc);
  }
  instructions.data[instructions.size] = instr;
  instructions.size++;
  if (lab && current_label[0] != '\0') {
    n = strlen(current_label);
    instructions.labels[instructions.size-1] = 
      (char*)malloc((n+1) * sizeof(char));
    strcpy(instructions.labels[instructions.size-1], current_label);
  }
  else 
    instructions.labels[instructions.size-1] = '\0';
  current_label[0] = '\0';
  return instr;
}

/* _________________________________________________________________ */
u_int 
RCUC_current_address() 
{
  return instructions.size;
}

/* _________________________________________________________________ */
void RCUC_print(const struct RCUC_set *instr, FILE* file, 
		short nums, short labs) 
{
  u_int i;
  if (!instr) return;
  for (i = 0; i < instr->size; i++) {
    const char* lab = instr->labels[i]; 
    /* RCUC_find_label_by_address(i); */
    if (nums && labs)
      fprintf(file, "%3d  %16s 0x%08x\n", i, (lab ? lab : " "), 
	      instr->data[i]);
    else if (nums) 
      fprintf(file, "%3d 0x%08x\n", i, instr->data[i]);
    else if (labs) 
      fprintf(file, "%16s 0x%08x\n", (lab ? lab : " "), instr->data[i]);
    else 
      fprintf(file, "0x%08x\n", instr->data[i]);
  }
}

/* ================================================================= */
/** RCU_Code
    @ingroup rcu
 */
enum RCU_Code {
  /** 0x0 */
  RCU_JUMP, 
  /** 0x1 */
  RCU_RS_STATUS,
  /** 0x2 */
  RCU_RS_TRCFG,
  /** 0x3 */
  RCU_RS_TRCNT,
  /** 0x4 */
  RCU_dummy4,
  /** 0x5 */
  RCU_dummy5,
  /** 0x6 */
  RCU_CHRDO,
  /** 0x7 */
  RCU_PMREAD,
  /** 0x8 */
  RCU_PMWRITE,
  /** 0x9 */
  RCU_END,
  /** 0xa */
  RCU_WAIT, 
  /** 0xb */
  RCU_TRIGGER
};

/* _________________________________________________________________ */
u_int
RCUC_RCU_encode(u_int instr) 
{
  return instr + (3 << 20);
}

/* _________________________________________________________________ */
static u_int
RCUC_Instr_encode(u_int c) 
{
  return (c << 16);
}

/* _________________________________________________________________ */
u_int 
RCUC_Data_encode(u_int d, u_int offset, u_int range) 
{
  if (d > range) {
    RCUC_error("Warning: data (%d) larger than allowed range %d", d, range);
    d = range;
  }
  return d << offset;
}

/* _________________________________________________________________ */
u_int
RCUC_LOOP_encode(u_int addr, u_int n) 
{
  u_int ret = RCUC_Instr_encode(RCU_JUMP);
  ret += (1 << 15);
  ret += RCUC_Data_encode(n, 8, 0x7f);
  ret += RCUC_Data_encode(addr, 0, 0xff);
  return RCUC_RCU_encode(ret);
}

/* _________________________________________________________________ */
u_int
RCUC_JUMP_encode(u_int addr) 
{
  u_int ret = RCUC_Instr_encode(RCU_JUMP);
  ret += RCUC_Data_encode(addr, 0, 0xff);
  return RCUC_RCU_encode(ret);
}

/* _________________________________________________________________ */
u_int
RCUC_RCU_RS_STATUS_encode() 
{
  u_int ret = RCUC_Instr_encode(RCU_RS_STATUS);
  return ret;
}
  
/* _________________________________________________________________ */
u_int
RCUC_RCU_RS_TRCFG_encode() 
{
  u_int ret = RCUC_Instr_encode(RCU_RS_TRCFG);
  return ret;
}

/* _________________________________________________________________ */
u_int
RCUC_RCU_RS_TRCNT_encode() 
{
  u_int ret = RCUC_Instr_encode(RCU_RS_TRCNT);
  return ret;
}

/* _________________________________________________________________ */
u_int
RCUC_RCU_CHRDO_encode() 
{
  u_int ret = RCUC_Instr_encode(RCU_CHRDO);
  return ret;
}

/* _________________________________________________________________ */
u_int
RCUC_RCU_PMREAD_encode(u_int addr) 
{
  u_int ret = RCUC_Instr_encode(RCU_PMREAD);
  ret += RCUC_Data_encode(addr, 0, 0xfff);
  return ret;
}

/* _________________________________________________________________ */
u_int
RCUC_RCU_PMWRITE_encode(u_int addr, u_int broadcast) 
{
  u_int ret = RCUC_Instr_encode(RCU_PMWRITE);
  ret += RCUC_Data_encode(addr, 0, 0xfff);
  if (broadcast) ret += RCUC_Data_encode(1, 12, 0x1);
  return ret;
}

/* _________________________________________________________________ */
u_int
RCUC_END_encode() 
{
  u_int ret = (3 << 20) + RCUC_Instr_encode(RCU_END);
  return ret;
}

/* _________________________________________________________________ */
u_int
RCUC_RCU_WAIT_encode(u_int delay) 
{
  u_int ret = RCUC_Instr_encode(RCU_WAIT);
  ret += RCUC_Data_encode(delay, 0, 0xff);
  return ret;
}

/* _________________________________________________________________ */
u_int
RCUC_RCU_TRIGGER_encode() 
{
  u_int ret = RCUC_Instr_encode(RCU_TRIGGER);
  return ret;
}

/* _________________________________________________________________ */
u_int
RCUC_OFFSET_resolve(int offset) 
{
  if (offset < 0 && -offset > RCUC_current_address()) {
    RCUC_error("Error: offset %d is before start of set",offset);
    exit(0);
  }
  return  RCUC_current_address() + offset;
}
  
/* _________________________________________________________________ */
u_int
RCUC_LABEL_resolve() 
{
  long a = RCUC_find_label(current_label);
  if (a < 0) { 
    RCUC_error("Error: label %s not found", current_label); 
    exit(1);
  }
  return a;
}

/* ================================================================ */
/** ALTRO instruction codes 
    @ingroup altro
 */
enum ALTRO_Code
{
  /** 0x0 */
  ALTRO_K1, 
  /** 0x01 */
  ALTRO_K2, 
  /** 0x02 */
  ALTRO_K3, 
  /** 0x03 */
  ALTRO_L1, 
  /** 0x04 */
  ALTRO_L2, 
  /** 0x05 */
  ALTRO_L3, 
  /** 0x06 */
  ALTRO_VFPED, 
  /** 0x07 */
  ALTRO_PMDTA, 
  /** 0x08 */
  ALTRO_ZSTHR, 
  /** 0x09 */
  ALTRO_BCTHR, 
  /** 0x0A */
  ALTRO_TRCFG, 
  /** 0x0B */
  ALTRO_DPCFG, 
  /** 0x0C */
  ALTRO_BFNPT, 
  /** 0x0D */
  ALTRO_PMADD, 
  /** 0x0E */
  ALTRO_dummy_0e, 
  /** 0x0F */
  ALTRO_dummy_0f, 
  /** 0x10 */
  ALTRO_ERSTR, 
  /** 0x11 */
  ALTRO_ADEVL, 
  /** 0x12 */
  ALTRO_TRCNT, 
  /** 0x13 */
  ALTRO_dummy_13,
  /** 0x14 */
  ALTRO_dummy_14,
  /** 0x15 */
  ALTRO_dummy_15,
  /** 0x16 */
  ALTRO_dummy_16,
  /** 0x17 */
  ALTRO_dummy_17,
  /** 0x18 */
  ALTRO_WPINC,
  /** 0x19 */
  ALTRO_RPINC,
  /** 0x1A */
  ALTRO_CHRDO,
  /** 0x1B */
  ALTRO_SWTRG,
  /** 0x1C */
  ALTRO_TRCLR,
  /** 0x1D */
  ALTRO_ERCLR
};

static u_int altro_data = 0;
/* _________________________________________________________________ */
u_int
RCUC_ALTRO_2nd_encode(u_int instr) 
{
  return instr + (1 << 22) + (1 << 20);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_encode(u_int instr) 
{
  u_int next = RCUC_ALTRO_2nd_encode(altro_data);
  RCUC_add_instruction(instr + (1 << 22), 1);
  altro_data = 0;
  return next;
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_read_encode(u_int instr) 
{
  return instr;
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_write_encode(u_int instr) 
{
  return instr + (1 << 21);
}

/* _________________________________________________________________ */
u_int 
RCUC_BROADCAST_encode() 
{
  return 1 << 18;
}

/* _________________________________________________________________ */
u_int 
RCUC_BOARD_CHIP_CHANNEL_encode(u_int board, u_int chip, u_int ch) 
{
  return board + chip + ch;
}

/* _________________________________________________________________ */
u_int 
RCUC_BOARD_CHIP_encode(u_int board, u_int chip) 
{
  return board + chip;
}

/* _________________________________________________________________ */
u_int 
RCUC_CHANNEL_encode(u_int ch) 
{
  return RCUC_Data_encode(ch, 5, 0xf);
}

/* _________________________________________________________________ */
u_int 
RCUC_CHIP_encode(u_int chip) 
{
  return RCUC_Data_encode(chip, 9, 0x7);
}

/* _________________________________________________________________ */
u_int 
RCUC_BOARD_encode(u_int board) 
{
  return RCUC_Data_encode(board, 12, 0x1f);
}

/* _________________________________________________________________ */
u_int 
RCUC_ALTRO_K_read_encode(u_short which, u_int addr) 
{
  u_int ret, incode = 0;
  switch (which) {
  case 1: incode = ALTRO_K1; break;
  case 2: incode = ALTRO_K2; break;
  case 3: incode = ALTRO_K3; break;
  default:
    RCUC_error("Unknown K constant %d", which);
    exit(1);
  }
  ret = incode + addr;
  return ret;
}
/* _________________________________________________________________ */
u_int 
RCUC_ALTRO_K_write_encode(u_short which, u_int addr, u_int data) 
{
  u_int ret, incode = 0;
  switch (which) {
  case 1: incode = ALTRO_K1; break;
  case 2: incode = ALTRO_K2; break;
  case 3: incode = ALTRO_K3; break;
  default:
    RCUC_error("Unknown K constant %d", which);
    exit(1);
  }
  ret        = RCUC_ALTRO_write_encode(incode + addr);
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data, 0, 0xffff));
  return ret;
}
/* _________________________________________________________________ */
u_int 
RCUC_ALTRO_L_read_encode(u_short which, u_int addr) 
{
  u_int ret, incode = 0;
  switch (which) {
  case 1: incode = ALTRO_L1; break;
  case 2: incode = ALTRO_L2; break;
  case 3: incode = ALTRO_L3; break;
  default:
    RCUC_error("Unknown L constant %d", which);
    exit(1);
  }
  ret = incode + addr;
  return ret;
}
/* _________________________________________________________________ */
u_int 
RCUC_ALTRO_L_write_encode(u_short which, u_int addr, u_int data) 
{
  u_int ret, incode = 0;
  switch (which) {
  case 1: incode = ALTRO_L1; break;
  case 2: incode = ALTRO_L2; break;
  case 3: incode = ALTRO_L3; break;
  default:
    RCUC_error("Unknown L constant %d", which);
    exit(1);
  }
  ret        = RCUC_ALTRO_write_encode(incode + addr);
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data, 0, 0xffff));
  return ret;
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_VFPED_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + ALTRO_VFPED);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_VFPED_write_encode(u_int addr, u_int data)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data, 0, 0x3ff));
  return RCUC_ALTRO_write_encode(addr + ALTRO_VFPED);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_PMDTA_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + ALTRO_PMDTA);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_PMDTA_write_encode(u_int addr, u_int data)
{
  altro_data = RCUC_Data_encode(data, 0, 0x3ff);
  return RCUC_ALTRO_write_encode(addr + ALTRO_PMDTA);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_ZSTHR_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + ALTRO_ZSTHR);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_ZSTHR_write_encode(u_int addr, u_int off, u_int thr)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(off, 10, 0x3ff) 
				       + RCUC_Data_encode(thr, 0, 0x3ff));
  return RCUC_ALTRO_write_encode(addr + ALTRO_ZSTHR);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_BCTHR_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + ALTRO_BCTHR);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_BCTHR_write_encode(u_int addr, u_int high, u_int low)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(high, 10, 0x3ff) 
				       + RCUC_Data_encode(low, 0, 0x3ff));
  return RCUC_ALTRO_write_encode(addr + ALTRO_BCTHR);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_TRCFG_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + ALTRO_TRCFG);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_TRCFG_write_encode(u_int addr, u_int start, u_int end)
{
  if (start > end) {
    RCUC_error("Error: Acquisition start %d after end %d", start, end);
    exit(1);
  }
  if (end > 0x1fa) 
    RCUC_error("Warning: Acquisition end is larger than 0x1FA, "
	       "which can be a problem");
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(start, 10, 0x3f0) 
				       + RCUC_Data_encode(end, 0, 0x3f0));
  return RCUC_ALTRO_write_encode(addr + ALTRO_TRCFG);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_DPCFG_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + ALTRO_DPCFG);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_DPCFG_write_encode(u_int addr, u_int invert, u_int first_bl,
		   u_int second_bl, u_int zero_sup)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(invert, 4, 1) 
				       + first_bl + second_bl + zero_sup);
  return RCUC_ALTRO_write_encode(addr + ALTRO_DPCFG);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_FIRST_BASELINE_encode(u_int mode) 
{
  return RCUC_Data_encode(mode, 0, 0xf);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_SECOND_BASELINE_encode(u_int pre, u_int post) 
{
  return (RCUC_Data_encode(pre, 5, 3) + 
	  RCUC_Data_encode(post, 7, 0xf) +
	  (1 << 11));
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_ZERO_SUPPRESSION_encode(u_int conf, u_int post, u_int pre) 
{
  return (RCUC_Data_encode(conf, 12, 3) + 
	  RCUC_Data_encode(post, 14, 7) +
	  RCUC_Data_encode(pre,  17, 3) +
	  (1 << 19));
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_BFNPT_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + ALTRO_BFNPT);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_BFNPT_write_encode(u_int addr, u_int pretrigger, u_int bufsize,
		   u_int filter, u_int powersave)
{
  u_int bs = 0;
  switch (bufsize) {
  case 0: bs = 0; break;
  case 4: bs = 0; break;
  case 8: bs = 1; break;
  default: 
    RCUC_error("Warning: invalid buffer size specified: %d (should be 4 or 8)",
	       bufsize);
    break;
  }
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(pretrigger, 0, 0xf) +
				       bs * (1 << 4) + filter * (1 << 5) +
				       powersave * (1 << 6));
  return RCUC_ALTRO_write_encode(addr + ALTRO_BFNPT);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_PMADD_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + ALTRO_PMADD);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_PMADD_write_encode(u_int addr, u_int data)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data, 0, 0x3ff));
  return RCUC_ALTRO_write_encode(addr + ALTRO_PMADD);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_ERSTR_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + ALTRO_ERSTR);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_ADEVL_read_encode(u_int chip, u_int ch)
{
  return RCUC_ALTRO_read_encode(chip + ch + ALTRO_ADEVL);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_TRCNT_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + ALTRO_TRCNT);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_WPINC_cmd_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + ALTRO_WPINC);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_RPINC_cmd_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + ALTRO_RPINC);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_CHRDO_cmd_encode(u_int chip, u_int ch)
{
  return RCUC_ALTRO_read_encode(chip + ch + ALTRO_CHRDO);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_SWTRG_cmd_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + ALTRO_SWTRG);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_TRCLR_cmd_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + ALTRO_TRCLR);
}

/* _________________________________________________________________ */
u_int
RCUC_ALTRO_ERCLR_cmd_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + ALTRO_ERCLR);
}


/* ================================================================ */
/** BC instruction codes 
    @ingroup bc
 */
enum BC_Code
{
  /** 0x0 */
  BC_dummy_0,
  /** 0x1 */
  BC_T_TH,
  /** 0x2 */
  BC_AV_TH, 
  BC_AC_TH, 
  BC_DV_TH,
  BC_DC_TH, 
  BC_T, 
  BC_AV, 
  BC_AC, 
  BC_DV, 
  BC_DC,
  BC_L1CNT, 
  BC_L2CNT, 
  BC_SCLKCNT, 
  BC_DSTBCNT, 
  BC_TSMWORD, 
  BC_USRATIO, 
  BC_CSR0, 
  BC_CSR1, 
  BC_CSR2, 
  BC_CSR3, 
  BC_FMD,
  BC_CNTLAT, 
  BC_CNTCLR, 
  BC_CSR1CLR, 
  BC_ALRST, 
  BC_BCRST, 
  BC_STCNV, 
  BC_SCEVL, 
  BC_EVLRDO, 
  BC_STTSM, 
  BC_ACQRDO
};
  
  
/* _________________________________________________________________ */
u_int
RCUC_BC_encode(u_int instr) 
{
  return RCUC_ALTRO_encode(instr + (1 << 17));
}

/* _________________________________________________________________ */
u_int
RCUC_BC_T_TH_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_T_TH);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_T_TH_write_encode(u_int addr, u_int data)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data, 0, 0x3ff));
  return RCUC_ALTRO_write_encode(addr + BC_T_TH);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_AV_TH_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_AV_TH);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_AV_TH_write_encode(u_int addr, u_int data)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data, 0, 0x3ff));
  return RCUC_ALTRO_write_encode(addr + BC_AV_TH);
}


/* _________________________________________________________________ */
u_int
RCUC_BC_AC_TH_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_AC_TH);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_AC_TH_write_encode(u_int addr, u_int data)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data, 0, 0x3ff));
  return RCUC_ALTRO_write_encode(addr + BC_AC_TH);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_DV_TH_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_DV_TH);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_DV_TH_write_encode(u_int addr, u_int data)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data, 0, 0x3ff));
  return RCUC_ALTRO_write_encode(addr + BC_DV_TH);
}


/* _________________________________________________________________ */
u_int
RCUC_BC_DC_TH_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_DC_TH);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_DC_TH_write_encode(u_int addr, u_int data)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data, 0, 0x3ff));
  return RCUC_ALTRO_write_encode(addr + BC_DC_TH);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_T_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_T);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_AV_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_AV);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_AC_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_AC);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_DV_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_DV);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_DC_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_DC);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_L1CNT_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_L1CNT);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_L2CNT_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_L2CNT);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_SCLKCNT_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_SCLKCNT);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_DSTBCNT_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_DSTBCNT);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_TSMWORD_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_TSMWORD);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_TSMWORD_write_encode(u_int addr, u_int data)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data, 0, 0x1ff));
  return RCUC_ALTRO_write_encode(addr + BC_TSMWORD);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_USRATIO_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_USRATIO);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_USRATIO_write_encode(u_int addr, u_int data)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data, 0, 0xffff));
  return RCUC_ALTRO_write_encode(addr + BC_USRATIO);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_CSR_read_encode(u_int which, u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_CSR0 + which);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_CSR_write_encode(u_int which, u_int addr, u_int data)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data, 0, 0xffff));
  return RCUC_ALTRO_write_encode(addr + BC_CSR0 + which);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_CNTLAT_cmd_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_CNTLAT);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_CNTCLR_cmd_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_CNTCLR);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_CSR1CLR_cmd_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_CSR1CLR);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_ALRST_cmd_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_ALRST);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_BCRST_cmd_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_BCRST);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_STCNV_cmd_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_STCNV);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_SCEVL_cmd_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_SCEVL);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_EVLRDO_cmd_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_EVLRDO);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_STTSM_cmd_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_STTSM);
}

/* _________________________________________________________________ */
u_int
RCUC_BC_ACQRDO_cmd_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + BC_ACQRDO);
}


/* ================================================================ */
enum FMD_Code 
{ 
  FMD_BIAS,        //  0 0x015
  FMD_VFP,         //  1 0x035 
  FMD_VFS,         //  2 0x055 
  FMD_CAL_LVL,     //  3 0x075 
  FMD_SHIFT_CLK,   //  4 0x095 
  FMD_SAMPLE_CLK,  //  5 0x0b5 
  FMD_HOLD_DELAY,  //  6 0x0d5 
  FMD_L1_TIMEOUT,  //  7 0x0f5 
  FMD_L2_TIMEOUT,  //  8 0x115 
  FMD_RANGE,       //  9 0x135 
  FMD_CMD,         // 10 0x155 
  FMD_dummy_11,    // 11 0x175 
  FMD_L0,          // 12 0x195 
  FMD_dummy_14,    // 14 0x1d5 
  FMD_dummy_u      // 15 0x1f5 
};		       
		       
#define FMD_ADDR 5     
		       
/* _________________________________________________________________ */
u_int
RCUC_FMD_BIAS_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + FMD_BIAS);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_BIAS_write_encode(u_int addr, u_int data0, u_int data1)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data1, 8, 0xff) +
				       RCUC_Data_encode(data0, 0, 0xff));
  return RCUC_ALTRO_write_encode(addr + FMD_BIAS);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_VFP_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + FMD_VFP);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_VFP_write_encode(u_int addr, u_int data0, u_int data1)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data1, 8, 0xff) +
				       RCUC_Data_encode(data0, 0, 0xff));
  return RCUC_ALTRO_write_encode(addr + FMD_VFP);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_VFS_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + FMD_VFS);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_VFS_write_encode(u_int addr, u_int data0, u_int data1)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data1, 8, 0xff) +
				       RCUC_Data_encode(data0, 0, 0xff));
  return RCUC_ALTRO_write_encode(addr + FMD_VFS);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_CAL_LVL_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + FMD_CAL_LVL);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_CAL_LVL_write_encode(u_int addr, u_int data0, u_int data1)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data1, 8, 0xff) +
				       RCUC_Data_encode(data0, 0, 0xff));
  return RCUC_ALTRO_write_encode(addr + FMD_CAL_LVL);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_SHIFT_CLK_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + FMD_SHIFT_CLK);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_SHIFT_CLK_write_encode(u_int addr, u_int data0, u_int data1)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data1, 8, 0xff) +
				       RCUC_Data_encode(data0, 0, 0xff));
  return RCUC_ALTRO_write_encode(addr + FMD_SHIFT_CLK);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_SAMPLE_CLK_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + FMD_SAMPLE_CLK);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_SAMPLE_CLK_write_encode(u_int addr, u_int data0, u_int data1)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data1, 8, 0xff) +
				       RCUC_Data_encode(data0, 0, 0xff));
  return RCUC_ALTRO_write_encode(addr + FMD_SAMPLE_CLK);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_HOLD_DELAY_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + FMD_HOLD_DELAY);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_HOLD_DELAY_write_encode(u_int addr, u_int data0)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data0, 0, 0xffff));
  return RCUC_ALTRO_write_encode(addr + FMD_HOLD_DELAY);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_L1_TIMEOUT_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + FMD_L1_TIMEOUT);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_L1_TIMEOUT_write_encode(u_int addr, u_int data0)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data0, 0, 0xffff));
  return RCUC_ALTRO_write_encode(addr + FMD_L1_TIMEOUT);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_L2_TIMEOUT_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + FMD_L2_TIMEOUT);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_L2_TIMEOUT_write_encode(u_int addr, u_int data0)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data0, 0, 0xffff));
  return RCUC_ALTRO_write_encode(addr + FMD_L2_TIMEOUT);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_RANGE_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + FMD_RANGE);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_RANGE_write_encode(u_int addr, u_int data0, u_int data1)
{
  altro_data = RCUC_ALTRO_write_encode(RCUC_Data_encode(data1, 8, 0xff) +
				       RCUC_Data_encode(data0, 0, 0xff));
  return RCUC_ALTRO_write_encode(addr + FMD_RANGE);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_cmd_encode(u_int cmd, u_int addr)
{
  altro_data = RCUC_ALTRO_write_encode(cmd);
  return RCUC_ALTRO_write_encode(addr + FMD_CMD);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_CHG_DAC_cmd_encode()
{
  return RCUC_Data_encode(1, 0, 0xffff);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_TRIGGER_cmd_encode()
{
  return RCUC_Data_encode(1, 1, 0xffff);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_PULSER_MODE_cmd_encode(u_int enable)
{
  if (enable) 
     return RCUC_Data_encode(1, 4, 0xffff);
  return RCUC_Data_encode(1, 5, 0xffff);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_L0_read_encode(u_int addr)
{
  return RCUC_ALTRO_read_encode(addr + FMD_L0);
}

/* _________________________________________________________________ */
u_int
RCUC_FMD_encode(u_int instr) 
{
  return RCUC_BC_encode(BC_FMD + instr);
}


  
/* ================================================================ */
static char  current_input[1024];
static u_int current_lineno = 0;

void 
RCUC_lineno_increment() 
{
  current_lineno++;
}

/* _________________________________________________________________ */
u_int
RCUC_error(const char* format, ...)
{
  static char buffer[1024];
  va_list ap;
  va_start(ap, format);
  vsnprintf(buffer, 1024, format, ap);
  if (RCUC_use_stderr) 
    fprintf(stderr, "In input '%s', line %d: %s\n", 
	    current_input, current_lineno, buffer);
  else 
    snprintf(RCUC_last_error, 1024, "In input '%s', line %d: %s", 
	     current_input, current_lineno, buffer);
  return 1;
}

/* _________________________________________________________________ */
const char*
RCUC_get_error()
{
  return RCUC_last_error;
}

/* _________________________________________________________________ */
struct RCUC_set*
RCUC_compile(const char* filename, int trace, int output)
{
  int ret;
  RCUC_use_stderr = output;
  RCUC_last_error[0] = '\0';
  RCUC_free(&instructions);
  strcpy(current_input, filename);
  current_lineno = 1;
  current_label[0] = '\0';
  if (filename[0] == '-' && filename[1] == '\0') 
    yyin = stdin;
  else 
    yyin = fopen(filename, "r");
  if (!yyin) {
    RCUC_error("Failed to open input file '%s': %s", 
	       current_input, strerror(errno));
    return 0;
  }
  /* yydebug = 1; */
  yydebug = trace;
  ret = yyparse();
  if (ret) {
    fprintf(stderr, "Failed\n");
    return 0;
  }
  // RCUC_print();
  return &instructions;
}

/* _________________________________________________________________ */
void
RCUC_free(struct RCUC_set* s) 
{
  int i = 0;
  if (!s) return;
  for (i = 0; i < s->size; i++)
    if (s->labels[i]) free(s->labels[i]);
  free (s->data);
  free (s->labels);
  s->size  = 0;
  s->data  = 0;
  s->labels = 0;
  // s->alloc = 0;
}


/*
 *
 */
