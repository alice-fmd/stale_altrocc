/* 
 *
 * Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307 USA
 */
%{
  /* Declarations */
#include <compiler.h>
#ifndef YY_DECL
  extern int yylex();
#endif
  static int yyerror(const char* msg);
%}

%token  INTEGER		/* */
%token  PLUS		/* */
%token  MINUS		/* */
%token  COLON		/* */
%token  NAME

%token ZERO		/* ZERO		[Zz][Ee][Rr][Oo]*/
%token WRITE		/* WRITE	[Ww][Rr][Ii][Tt][Ee]*/
%token WORDS            /* WORDS        [Ww][Oo][Rr][Dd][Ss]*/
%token WAIT		/* WAIT		[Ww][Aa][Ii][Tt]*/
%token VOLTAGE          /* VOLTAGE      [Vv][Oo][Ll][Tt][Aa][Gg][Ee]*/
%token VFS              /* VFS          [Vv][Ff][Ss]*/
%token VFP              /* VFP          [Vv][Ff][Pp]*/
%token UNDER            /* UNDER        [Uu][Nn][Dd][Ee][Rr]*/
%token TWO              /* TWO          [Tt][Ww][Oo] */
%token TRIGGER		/* TRIGGER	[Tt][Rr][Ii][Gg][Gg][Ee][Rr]*/
%token THRESHOLD        /* THRESHOLD    [Tt][Hh][Rr][Ee][Ss][Hh][Oo][Ll][Dd]*/
%token TIMEOUT          /* TIMEOUT      [Tt][Ii][Mm][Ee][Oo][Uu][Tt]*/
%token THIRD            /* THIRD        [Tt][Hh][Ii][Rr][Dd]*/
%token TEMPERATURE      /* TEMPERATURE  [Tt][Ee][Mm][Pp][Ee][Rr][Aa][Tt][Uu][Rr][Ee]*/

%token TEST             /* TEST         [Tt][Ee][Ss][Tt]*/
%token SUPPRESSION	/* SUPPRESSION	[Ss][Uu][Pp][Pp][Rr][Ee][Ss][Ss][Ii][Oo][Nn]*/
%token STROBE           /* STROBE       [Ss][Tt][Rr][Op][Bb][Ee]*/
%token STATUS		/* STATUS	[Ss][Tt][Aa][Tt][Uu][Ss]*/
%token SOFTWARE		/* SOFTWARE	[Ss][Oo][Ff][Tt][Ww][Aa][Rr][Ee]*/
%token SIZE		/* SIZE		[Ss][Ii][Zz][Ee]*/
%token SHIFT            /* SHIFT        [Ss][Hh][Ii][Ff][Tt]*/
%token SECOND		/* SECOND	[Ss][Ee][Cc][Oo][Nn][Dd]*/
%token SCAN             /* SCAN         [Ss][Cc][Aa][Nn]*/
%token SAVE		/* SAVE		[Ss][Aa][Vv][Ee]*/
%token SAMPLE           /* SAMPLE       [Ss][Aa][Mm][Pp][Ll][Ee]*/
%token RESET		/* RESET	[Rr][Ee][Ss][Ee][Tt]*/
%token READOUT		/* READOUT	[Rr][Ee][Aa][Dd][Oo][Uu][Tt]*/
%token READ		/* READ		[Rr][Ee][Aa][Dd]*/
%token RCU		/* RCU		[Rr][Cc][Uu]*/
%token RANGE            /* RANGE        [Rr][Aa][Nn][Gg][Ee]*/
%token PULSER           /* PULSER       [Pp][Uu][Ll][Ss][Ee][Rr]*/
%token PRE		/* PRE		[Pp][Rr][Ee]*/
%token POWER		/* POWER	[Pp][Oo][Ww][Ee][Rr]*/
%token PEDESTAL		/* PEDESTAL	[Pp][Ee][Dd][Ee][Ss][Tt][Aa][Ll]*/
%token PATH		/* PATH		[Pp][Aa][Tt][Hh]*/
%token ONE              /* ONE          [Oo][Nn][Ee]*/
%token ON               /* ON           [Oo][Nn]*/
%token OFF              /* OFF          [Oo][Ff][Ff]*/
%token MONITOR          /* MONITOR      [Mm][Oo][Nn][Ii][Tt][Oo][Rr]*/
%token MODE             /* MODE         [Mm][Oo][Dd][Ee]*/
%token LOOP		/* LOOP		[Ll][Oo][Oo][Pp]*/
%token L3		/* L3		[Ll][33]*/
%token L2		/* L2		[Ll][22]*/
%token L1		/* L1		[Ll][11]*/
%token LENGTH           /* LENGTH       [Ll][Ee][Nn][Gg][Tt][Hh]*/
%token LATCH            /* LATCH        [Ll][Aa][Tt][Cc][Hh]*/
%token K3		/* K3		[Kk][33]*/
%token K2		/* K2		[Kk][22]*/
%token K1		/* K1		[Kk][11]*/
%token JUMP		/* JUMP		[Jj][Uu][Mm][Pp]*/
%token INVERT		/* INVERT	[Ii][Nn][Vv][Ee][Rr][Tt]*/
%token INCREMENT	/* INCREMENT	[Ii][Nn][Cc][Rr][Ee][Mm][Ee][Nn][Tt]*/
%token HOLD             /* HOLD         [Hh][Oo][Ll][Dd]*/
%token GROUP		/* GROUP	[Gg][Rr][Oo][Uu][Pp]*/
%token FIRST		/* FIRST	[Ff][Ii][Rr][Ss][Tt]*/
%token FILTER		/* FILTER	[Ff][Ii][Ll][Tt][Ee][Rr]*/
%token FMD              /* FMD          [Ff][Mm][Dd]*/
%token EVENT            /* EVENT        [Ee][Vv][Ee][Nn][Tt]*/
%token ERROR		/* ERROR	[Ee][Rr][Rr][Oo][Rr]*/
%token END		/* END		[Ee][Nn][Dd]*/
%token DATA             /* DATA         [Dd][Aa][Tt][Aa]*/
%token DIGITAL          /* DIGITAL      [Dd][Ii][Gg][Ii][Tt][Aa][Ll]*/
%token DELAY            /* DELAY        [Dd][Ee][Ll][Aa][Yy]*/
%token CURRENT          /* CURRENT      [Cc][Uu][Rr][Rr][Ee][Nn][Tt]*/
%token COUNTER		/* COUNTER	[Cc][Oo][Uu][Nn][Tt][Ee][Rr]*/
%token CONFIG		/* CONFIG	[Cc][Oo][Nn][Ff][Ii][Gg]*/
%token CLOCK            /* CLOCK        [Cc][Ll][Oo][Cc][Kk]*/
%token CHANGE           /* CHANGE       [Cc][Hh][Aa][Nn][Gg][Ee]*/
%token BUFFER		/* BUFFER	[Bb][Uu][Ff][Ff][Ee][Rr]*/
%token BROADCAST	/* BROADCAST    [Bb][Rr][Oo][Aa][Dd][Cc][Aa][Ss][Tt]*/
%token BIAS             /* BIAS         [Bb][Ii][Aa][Ss]*/
%token BC               /* BC           [Bb][Cc]*/
%token BASELINE		/* BASELINE	[Bb][Aa][Ss][Ee][Ll][Ii][Nn][Ee]*/
%token ANALOG           /* ANALOG       [Aa][Nn][Aa][Ll][Oo][Gg]*/
%token ALTRO		/* ALTRO	[Aa][Ll][Tt][Rr][Oo]*/
%token ADDRESS		/* ADDRESS	[Aa][Dd][Dd][Rr][Ee][Ss][Ss]*/


%error-verbose
%%
/* -------------------------------------------------------------------
 *
 * Grammar 
 */
input	
  : /* Nothing */		
  | input group		
  ;

/* -------------------------------------------------------------------
 * Groups */
group	
  : GROUP list labeled_end	
  ;

labeled_end
  : END			{ $$ = RCUC_add_instruction(RCUC_END_encode(), 0); } 
  | NAME COLON END	{ $$ = RCUC_add_instruction(RCUC_END_encode(), 1); }
  ;


list	
  : labeled_instruction			{ $$ = $1; }
  | list labeled_instruction		{ $$ = $1; }
  ;

labeled_instruction
  : instruction           		{ $$ = RCUC_add_instruction($1, 0); }
  | NAME COLON instruction		{ $$ = RCUC_add_instruction($3, 1);  }
  ;


address
  : INTEGER				{ $$ = $1;  }
  | PLUS INTEGER			{ $$ = RCUC_OFFSET_resolve(+ $2); }
  | MINUS INTEGER			{ $$ = RCUC_OFFSET_resolve(- $2); }
  | NAME				{ $$ = RCUC_LABEL_resolve(); }
  ;

instruction
  : loop_instruction			{ $$ = $1; }
  | jump_instruction			{ $$ = $1; }
  | RCU rcu_instruction 		{ $$ = RCUC_RCU_encode($2); } 
  | ALTRO altro_instruction 		{ $$ = RCUC_ALTRO_encode($2); } 
  | BC bc_instruction                   { $$ = RCUC_BC_encode($2); }
  | FMD fmd_instruction                 { $$ = RCUC_FMD_encode($2); }
  ;

/* -------------------------------------------------------------------
 * General instructions */
loop_instruction
  : LOOP  address INTEGER 		{ $$ = RCUC_LOOP_encode($2, $3); }
  ;
jump_instruction
  : JUMP 	address			{ $$ = RCUC_JUMP_encode($2); }
  ;

/* -------------------------------------------------------------------
 * RCU instructions */
rcu_instruction
  : reset_instruction			{ $$ = $1; }
  | readout_instruction			{ $$ = $1; }
  | pedestal_instruction		{ $$ = $1; } 
  | wait_instruction 			{ $$ = $1; }
  | trigger_wait_instruction		{ $$ = $1; }
  ;

reset_instruction
  : RESET STATUS 			{ $$ = RCUC_RCU_RS_STATUS_encode(); }
  | RESET TRIGGER CONFIG 		{ $$ = RCUC_RCU_RS_TRCFG_encode(); }
  | RESET TRIGGER COUNTER 		{ $$ = RCUC_RCU_RS_TRCNT_encode(); }
  ;
readout_instruction
  : READOUT				{ $$ = RCUC_RCU_CHRDO_encode(); }
  ;

pedestal_instruction
  : READ  PEDESTAL INTEGER		{ $$ = RCUC_RCU_PMREAD_encode($3); }
  | WRITE PEDESTAL INTEGER 		{ $$ = RCUC_RCU_PMWRITE_encode($3, 0);}
  | WRITE PEDESTAL BROADCAST  		{ $$ = RCUC_RCU_PMWRITE_encode(0, 1);}
  ; 
wait_instruction
  : WAIT INTEGER			{ $$ = RCUC_RCU_WAIT_encode($2); }
  ;
trigger_wait_instruction
  : TRIGGER	 			{ $$ = RCUC_RCU_TRIGGER_encode(); }
  ;

/* -------------------------------------------------------------------
 * ALTRO instructions */
altro_instruction
  /* Local instructions */
  : k_instruction			{ $$ = $1; }
  | l_instruction			{ $$ = $1; }
  | vfped_instruction			{ $$ = $1; }
  | pmdta_instruction			{ $$ = $1; }
  | adevl_instruction			{ $$ = $1; }
  /* Global instructions */	
  | zsthr_instruction			{ $$ = $1; }
  | bcthr_instruction			{ $$ = $1; }
  | trcfg_instruction			{ $$ = $1; }
  | dpcfg_instruction			{ $$ = $1; }
  | bfnpt_instruction			{ $$ = $1; }
  | erstr_instruction			{ $$ = $1; }
  | trcnt_instruction			{ $$ = $1; }
  | pmadd_instruction			{ $$ = $1; }
  /* Commands */			
  | wpinc_instruction			{ $$ = $1; }
  | rpinc_instruction			{ $$ = $1; }
  | chrdo_instruction			{ $$ = $1; }
  | swtrg_instruction			{ $$ = $1; }
  | trclr_instruction			{ $$ = $1; }
  | erclr_instruction			{ $$ = $1; }
  ;

bcast_channel_address
  : BROADCAST				{ $$ = RCUC_BROADCAST_encode(); }
  | board_address chip_address channel_address 	
			{ $$ = RCUC_BOARD_CHIP_CHANNEL_encode($1,$2,$3); }
  ;

bcast_or_chip_address
  : BROADCAST				{ $$ = RCUC_BROADCAST_encode(); }
  | board_address chip_address		{ $$ = RCUC_BOARD_CHIP_encode($1,$2); }
  ;

bcast_or_board_address
  : BROADCAST				{ $$ = RCUC_BROADCAST_encode(); }
  | board_address			{ $$ = $1; }
  ; 

board_address
  : INTEGER				{ $$ = RCUC_BOARD_encode($1); }
  ;

chip_address
  : INTEGER				{ $$ = RCUC_CHIP_encode($1); }
  ;

channel_address 
  : INTEGER				{ $$ = RCUC_CHANNEL_encode($1); }
  ;

k_instruction
  : READ K1 bcast_channel_address 	
			{ $$ = RCUC_ALTRO_K_read_encode(1,$3); }
  | READ K2 bcast_channel_address 	
			{ $$ = RCUC_ALTRO_K_read_encode(2,$3); }
  | READ K3 bcast_channel_address 	
			{ $$ = RCUC_ALTRO_K_read_encode(3,$3); }
  | WRITE K1 bcast_channel_address INTEGER 
			{ $$ = RCUC_ALTRO_K_write_encode(1,$3,$4); }
  | WRITE K2 bcast_channel_address INTEGER 
			{ $$ = RCUC_ALTRO_K_write_encode(2,$3,$4); }
  | WRITE K3 bcast_channel_address INTEGER 
			{ $$ = RCUC_ALTRO_K_write_encode(3,$3,$4); }
  ;

l_instruction
  : READ L1 bcast_channel_address 	
			{ $$ = RCUC_ALTRO_L_read_encode(1,$3); }
  | READ L2 bcast_channel_address 	
			{ $$ = RCUC_ALTRO_L_read_encode(2,$3); }
  | READ L3 bcast_channel_address 	
			{ $$ = RCUC_ALTRO_L_read_encode(3,$3); }
  | WRITE L1 bcast_channel_address INTEGER 
			{ $$ = RCUC_ALTRO_L_write_encode(1,$3,$4); }
  | WRITE L2 bcast_channel_address INTEGER 
			{ $$ = RCUC_ALTRO_L_write_encode(2,$3,$4); }
  | WRITE L3 bcast_channel_address INTEGER 
			{ $$ = RCUC_ALTRO_L_write_encode(3,$3,$4); }
  ;

vfped_instruction
  : READ FIRST BASELINE bcast_channel_address 
			{ $$ =RCUC_ALTRO_VFPED_read_encode($4); }
  | WRITE FIRST BASELINE bcast_channel_address INTEGER 
    			{ $$ =RCUC_ALTRO_VFPED_write_encode($4, $5);}
  ;

pmdta_instruction
  : READ PEDESTAL bcast_channel_address 	
			{ $$ = RCUC_ALTRO_PMDTA_read_encode($3); }
  | WRITE PEDESTAL bcast_channel_address INTEGER 
    			{ $$ = RCUC_ALTRO_PMDTA_write_encode($3,$4);}
  ;

zsthr_instruction
  : READ ZERO SUPPRESSION bcast_or_chip_address 
			{ $$ = RCUC_ALTRO_ZSTHR_read_encode($4); }
  | WRITE ZERO SUPPRESSION bcast_or_chip_address INTEGER INTEGER 
    			{ $$ = RCUC_ALTRO_ZSTHR_write_encode($4, $5,$6);}
  ;

bcthr_instruction
  : READ SECOND BASELINE bcast_or_chip_address 
			{$$=RCUC_ALTRO_BCTHR_read_encode($4);}
  | WRITE SECOND BASELINE bcast_or_chip_address INTEGER INTEGER 
			{ $$ = RCUC_ALTRO_BCTHR_write_encode($4, $5, $6);}
  ;

trcfg_instruction
  : READ TRIGGER CONFIG bcast_or_chip_address 
			{ $$ = RCUC_ALTRO_TRCFG_read_encode($4); }
  | WRITE TRIGGER CONFIG bcast_or_chip_address INTEGER INTEGER 
    			{$$=RCUC_ALTRO_TRCFG_write_encode($4,$5,$6);}
  ;

dpcfg_instruction
  : READ  FIRST PATH bcast_or_chip_address 
			{ $$ = RCUC_ALTRO_DPCFG_read_encode($4); }
  | WRITE FIRST PATH bcast_or_chip_address invert_opt first_baseline_opt 
		     second_baseline_opt zero_suppression_opt
		{ $$ = RCUC_ALTRO_DPCFG_write_encode($4, $5, $6, $7, $8); }
  ;

invert_opt
  : /* Empty */			{ $$ = 0; }
  | INVERT			{ $$ = 1; } 
  ;

first_baseline_opt
  : /* Empty */			{ $$ = 0; }
  | FIRST BASELINE INTEGER 	{ $$ = RCUC_ALTRO_FIRST_BASELINE_encode($3);}
  ;

second_baseline_opt
  : /* Empty */				{ $$ = 0; }
  | SECOND BASELINE INTEGER INTEGER     
			{ $$ = RCUC_ALTRO_SECOND_BASELINE_encode($3, $4);}
  ;

zero_suppression_opt
  : /* Empty */				{ $$ = 0; }
  | ZERO SUPPRESSION INTEGER INTEGER INTEGER   
			{ $$ = RCUC_ALTRO_ZERO_SUPPRESSION_encode($3, $4, $5);}
  ;

bfnpt_instruction
  : READ  SECOND PATH bcast_or_chip_address 
			{ $$ = RCUC_ALTRO_BFNPT_read_encode($4); }
  | WRITE SECOND PATH bcast_or_chip_address pretrigger_opt
                      buffer_size_opt filter_enable_opt power_save_opt
		{ $$ = RCUC_ALTRO_BFNPT_write_encode($4, $5, $6, $7, $8); }
  ;

pretrigger_opt
  : /* Empty */				{ $$ = 0; }
  | PRE TRIGGER INTEGER			{ $$ = $3; }
  ;

buffer_size_opt
  : /* Empty */				{ $$ = 0; }
  | BUFFER SIZE INTEGER 		{ $$ = $3; }
  ;

filter_enable_opt
  : /* Empty */				{ $$ = 0; }
  | FILTER				{ $$ = 1; }
  ;

power_save_opt
  : /* Empty */				{ $$ = 0; }
  | POWER SAVE				{ $$ = 1; }
 
  ;

pmadd_instruction
  : READ  PEDESTAL ADDRESS bcast_or_chip_address 
			{ $$ = RCUC_ALTRO_PMADD_read_encode($4);}
  | WRITE PEDESTAL ADDRESS bcast_or_chip_address INTEGER
			{ $$ =RCUC_ALTRO_PMADD_write_encode($4, $5);}
  ;

erstr_instruction
  : READ ERROR chip_address 	{ $$ = RCUC_ALTRO_ERSTR_read_encode($3); }
  ;

adevl_instruction
  : READ ADDRESS chip_address channel_address 
			{ $$ = RCUC_ALTRO_ADEVL_read_encode($3,$4);}
  ;

trcnt_instruction
  : READ TRIGGER COUNTER bcast_or_chip_address 
			{ $$ = RCUC_ALTRO_TRCNT_read_encode($4); }
  ;

wpinc_instruction
  : WRITE INCREMENT bcast_or_chip_address 
			{ $$ = RCUC_ALTRO_WPINC_cmd_encode($3); }
  ;

rpinc_instruction
  : READ INCREMENT bcast_or_chip_address 
			{ $$ = RCUC_ALTRO_RPINC_cmd_encode($3); }
  ;

chrdo_instruction
  : READOUT chip_address channel_address 
			{ $$ = RCUC_ALTRO_CHRDO_cmd_encode($2,$3);}
  ;

swtrg_instruction
  : SOFTWARE TRIGGER bcast_or_chip_address 
			{ $$ = RCUC_ALTRO_SWTRG_cmd_encode($3); }
  ;

trclr_instruction
  : RESET TRIGGER COUNTER bcast_or_chip_address 
			{ $$ = RCUC_ALTRO_TRCLR_cmd_encode($4); }
  ;

erclr_instruction
  : RESET ERROR bcast_or_chip_address	
			{ $$ = RCUC_ALTRO_ERCLR_cmd_encode($3); }
  ;


/* -------------------------------------------------------------------
 * BC instructions */
bc_instruction
  : temperature_threshold_instruction		{ $$ = $1; }
  | analog_voltage_threshold_instruction	{ $$ = $1; } 
  | analog_current_threshold_instruction	{ $$ = $1; } 
  | digital_voltage_threshold_instruction	{ $$ = $1; } 
  | digital_current_threshold_instruction	{ $$ = $1; } 
  | temperature_instruction			{ $$ = $1; }
  | analog_voltage_instruction 			{ $$ = $1; }
  | analog_current_instruction 			{ $$ = $1; }
  | digital_voltage_instruction			{ $$ = $1; } 
  | digital_current_instruction			{ $$ = $1; }
  | l1cnt_instruction				{ $$ = $1; }
  | l2cnt_instruction				{ $$ = $1; }
  | sclkcnt_instruction				{ $$ = $1; }
  | dstbcnt_instruction				{ $$ = $1; }
  | tsmword_instruction				{ $$ = $1; }
  | usratio_instruction				{ $$ = $1; }
  | csr_instruction				{ $$ = $1; }
  | cntlat_instruction				{ $$ = $1; }
  | cntclr_instruction				{ $$ = $1; }
  | csr1clr_instruction				{ $$ = $1; }
  | alrst_instruction				{ $$ = $1; }
  | bcrst_instruction				{ $$ = $1; }
  | stcnv_instruction				{ $$ = $1; }
  | scevl_instruction				{ $$ = $1; }
  | evlrdo_instruction				{ $$ = $1; }
  | sttsm_instruction				{ $$ = $1; }
  | acqrdo_instruction				{ $$ = $1; }
  ;

temperature_threshold_instruction
  : READ TEMPERATURE THRESHOLD board_address 
			{ $$ = RCUC_BC_T_TH_read_encode($4); }
  | WRITE TEMPERATURE THRESHOLD bcast_or_board_address INTEGER
			{ $$ = RCUC_BC_T_TH_write_encode($4, $5); }
  ; 
analog_voltage_threshold_instruction
  : READ ANALOG VOLTAGE THRESHOLD board_address 
			{ $$ = RCUC_BC_AV_TH_read_encode($5); }
  | WRITE ANALOG VOLTAGE THRESHOLD bcast_or_board_address INTEGER
			{ $$ = RCUC_BC_AV_TH_write_encode($5, $6); }
  ;  
analog_current_threshold_instruction
  : READ ANALOG CURRENT THRESHOLD board_address 
			{ $$ = RCUC_BC_AC_TH_read_encode($5); }
  | WRITE ANALOG CURRENT THRESHOLD bcast_or_board_address INTEGER
			{ $$ = RCUC_BC_AC_TH_write_encode($5, $6); }
  ;  
digital_voltage_threshold_instruction
  : READ DIGITAL VOLTAGE THRESHOLD board_address 
			{ $$ = RCUC_BC_DV_TH_read_encode($5); }
  | WRITE DIGITAL VOLTAGE THRESHOLD bcast_or_board_address INTEGER
			{ $$ = RCUC_BC_DV_TH_write_encode($5, $6); }
  ;  
digital_current_threshold_instruction
  : READ DIGITAL CURRENT THRESHOLD board_address 
			{ $$ = RCUC_BC_DC_TH_read_encode($5); }
  | WRITE DIGITAL CURRENT THRESHOLD bcast_or_board_address INTEGER
			{ $$ = RCUC_BC_DC_TH_write_encode($5, $6); }
  ;  
temperature_instruction
  : READ TEMPERATURE board_address	{ $$ = RCUC_BC_T_read_encode($3); }
  ; 
analog_voltage_instruction
  : READ ANALOG VOLTAGE board_address	{ $$ = RCUC_BC_AV_read_encode($4); }
  ;  
analog_current_instruction
  : READ ANALOG CURRENT board_address	{ $$ = RCUC_BC_AC_read_encode($4); }
  ;  
digital_voltage_instruction
  : READ DIGITAL VOLTAGE board_address	{ $$ = RCUC_BC_DV_read_encode($4); }
  ;  
digital_current_instruction
  : READ DIGITAL CURRENT board_address	{ $$ = RCUC_BC_DC_read_encode($4); }
  ;  

l1cnt_instruction
  : READ TRIGGER ONE COUNTER board_address 
			{ $$ = RCUC_BC_L1CNT_read_encode($5); }
  ;
l2cnt_instruction
  : READ TRIGGER TWO COUNTER board_address 
			{ $$ = RCUC_BC_L2CNT_read_encode($5); }
  ;
sclkcnt_instruction
  : READ SAMPLE CLOCK COUNTER board_address 
			{ $$ = RCUC_BC_SCLKCNT_read_encode($5);}
  ;
dstbcnt_instruction
  : READ DATA STROBE COUNTER board_address 
			{ $$ = RCUC_BC_DSTBCNT_read_encode($5);}
  ;

tsmword_instruction
  : READ TEST MODE WORDS board_address { $$ = RCUC_BC_TSMWORD_read_encode($5); }
  | WRITE TEST MODE WORDS bcast_or_board_address INTEGER
			{ $$ = RCUC_BC_TSMWORD_write_encode($5, $6); }
  ;
usratio_instruction
  : READ UNDER SAMPLE board_address { $$ = RCUC_BC_USRATIO_read_encode($4); }
  | WRITE UNDER SAMPLE bcast_or_board_address INTEGER
			{ $$ = RCUC_BC_USRATIO_write_encode($4, $5); }
  ;

csr_instruction
  : READ ZERO   CONFIG board_address 
			{ $$ = RCUC_BC_CSR_read_encode(0, $4); }
  | READ FIRST  CONFIG board_address 
			{ $$ = RCUC_BC_CSR_read_encode(1, $4); }
  | READ SECOND CONFIG board_address 
			{ $$ = RCUC_BC_CSR_read_encode(2, $4); }
  | READ THIRD  CONFIG board_address 
			{ $$ = RCUC_BC_CSR_read_encode(3, $4); }
  | WRITE ZERO   CONFIG bcast_or_board_address INTEGER
			{ $$ = RCUC_BC_CSR_write_encode(0, $4, $5); }
  | WRITE FIRST  CONFIG bcast_or_board_address INTEGER
			{ $$ = RCUC_BC_CSR_write_encode(1, $4, $5); }
  | WRITE SECOND CONFIG bcast_or_board_address INTEGER
			{ $$ = RCUC_BC_CSR_write_encode(2, $4, $5); }
  | WRITE THIRD  CONFIG bcast_or_board_address INTEGER
			{ $$ = RCUC_BC_CSR_write_encode(3, $4, $5); }
  ;
cntlat_instruction
  : COUNTER LATCH bcast_or_board_address { $$ = RCUC_BC_CNTLAT_cmd_encode($3); }
  ;
cntclr_instruction
  : RESET COUNTER bcast_or_board_address { $$ = RCUC_BC_CNTCLR_cmd_encode($3); }
  ;
csr1clr_instruction
  : RESET STATUS bcast_or_board_address { $$ = RCUC_BC_CSR1CLR_cmd_encode($2); }
  ;
alrst_instruction
  : RESET ALTRO bcast_or_board_address { $$ = RCUC_BC_ALRST_cmd_encode($3); }
  ;
bcrst_instruction
  : RESET  bcast_or_board_address { $$ = RCUC_BC_BCRST_cmd_encode($2); }
  ;
stcnv_instruction
  : MONITOR bcast_or_board_address { $$ = RCUC_BC_STCNV_cmd_encode($2); }
  ;
scevl_instruction
  : SCAN EVENT LENGTH bcast_or_board_address 
			{ $$ = RCUC_BC_SCEVL_cmd_encode($4); }
  ;
evlrdo_instruction
  : READ EVENT LENGTH board_address { $$ = RCUC_BC_EVLRDO_cmd_encode($4);}
  ;
sttsm_instruction
  : TEST MODE bcast_or_board_address { $$ = RCUC_BC_STTSM_cmd_encode($3);}
  ;
acqrdo_instruction
  : READ EVENT DATA board_address { $$ = RCUC_BC_ACQRDO_cmd_encode($3);}
  ;

/* -------------------------------------------------------------------
 * FMD instructions */
fmd_instruction 
  : bias_instruction			{ $$ = $1; } 
  | vfp_instruction			{ $$ = $1; } 
  | vfs_instruction			{ $$ = $1; } 
  | cal_lvl_instruction			{ $$ = $1; } 
  | shift_clk_instruction		{ $$ = $1; } 
  | sample_clk_instruction		{ $$ = $1; } 
  | hold_delay_instruction 		{ $$ = $1; } 
  | l1_timeout_instruction 		{ $$ = $1; } 
  | l2_timeout_instruction 		{ $$ = $1; } 
  | range_instruction			{ $$ = $1; }
  | cmd_instruction                     { $$ = $1; }
  | l0_instruction                      { $$ = $1; }
  ;

bias_instruction
  : READ BIAS board_address	{ $$ = RCUC_FMD_BIAS_read_encode($3); }
  | WRITE BIAS bcast_or_board_address INTEGER 
			{ $$ = RCUC_FMD_BIAS_write_encode($3,$4,$4); }
  | WRITE BIAS bcast_or_board_address INTEGER INTEGER 
			{ $$ = RCUC_FMD_BIAS_write_encode($3,$4,$5); }
  ;
vfp_instruction
  : READ VFP board_address	{ $$ = RCUC_FMD_VFP_read_encode($3); }
  | WRITE VFP bcast_or_board_address INTEGER
			{ $$ = RCUC_FMD_VFP_write_encode($3,$4,$4); }
  | WRITE VFP bcast_or_board_address INTEGER INTEGER 
			{ $$ = RCUC_FMD_VFP_write_encode($3,$4,$5); }
  ;
vfs_instruction
  : READ VFS board_address	{ $$ = RCUC_FMD_VFS_read_encode($3); }
  | WRITE VFS bcast_or_board_address INTEGER
			{ $$ = RCUC_FMD_VFS_write_encode($3,$4,$4); }
  | WRITE VFS bcast_or_board_address INTEGER INTEGER 
			{ $$ = RCUC_FMD_VFS_write_encode($3,$4,$5); }
  ;
cal_lvl_instruction
  : READ PULSER board_address	{ $$ = RCUC_FMD_CAL_LVL_read_encode($3); }
  | WRITE PULSER bcast_or_board_address INTEGER
			{ $$ = RCUC_FMD_CAL_LVL_write_encode($3,$4, 0); }
  | WRITE PULSER bcast_or_board_address INTEGER INTEGER
			{ $$ = RCUC_FMD_CAL_LVL_write_encode($3,$4, $5); }
  ;
shift_clk_instruction
  : READ SHIFT CLOCK board_address { $$ = RCUC_FMD_SHIFT_CLK_read_encode($4); }
  | WRITE SHIFT CLOCK bcast_or_board_address INTEGER
			{ $$ = RCUC_FMD_SHIFT_CLK_write_encode($4,$5,0);}
  | WRITE SHIFT CLOCK bcast_or_board_address INTEGER INTEGER 
			{ $$ = RCUC_FMD_SHIFT_CLK_write_encode($4,$5,$6);}
  ;
sample_clk_instruction
  : READ SAMPLE CLOCK board_address 
			{ $$ = RCUC_FMD_SAMPLE_CLK_read_encode($4); }
  | WRITE SAMPLE CLOCK bcast_or_board_address INTEGER
			{ $$ = RCUC_FMD_SAMPLE_CLK_write_encode($4,$5,0);}
  | WRITE SAMPLE CLOCK bcast_or_board_address INTEGER INTEGER 
			{ $$ = RCUC_FMD_SAMPLE_CLK_write_encode($4,$5,$6);}
  ;
hold_delay_instruction
  : READ HOLD DELAY board_address { $$ = RCUC_FMD_HOLD_DELAY_read_encode($4); }
  | WRITE HOLD DELAY bcast_or_board_address INTEGER
			{ $$ = RCUC_FMD_HOLD_DELAY_write_encode($4,$5); }
  ;
l1_timeout_instruction
  : READ TRIGGER ONE TIMEOUT board_address 
			{ $$ = RCUC_FMD_L1_TIMEOUT_read_encode($5); }
  | WRITE TRIGGER ONE TIMEOUT bcast_or_board_address INTEGER
			{ $$ = RCUC_FMD_L1_TIMEOUT_write_encode($5,$6); }
  ;
l2_timeout_instruction
  : READ TRIGGER TWO TIMEOUT board_address 
			{ $$ = RCUC_FMD_L2_TIMEOUT_read_encode($5);}
  | WRITE TRIGGER TWO TIMEOUT bcast_or_board_address INTEGER
			{ $$ = RCUC_FMD_L2_TIMEOUT_write_encode($5,$6); }
  ;
range_instruction
  : READ RANGE board_address	{ $$ = RCUC_FMD_RANGE_read_encode($3); }
  | WRITE RANGE bcast_or_board_address INTEGER INTEGER 
			{ $$ = RCUC_FMD_RANGE_write_encode($3,$4,$5); }
  ;
cmd_instruction
  : fmd_cmd bcast_or_board_address { $$ = RCUC_FMD_cmd_encode($1,$2); }
  ;

fmd_cmd
  : CHANGE VOLTAGE      { $$ = RCUC_FMD_CHG_DAC_cmd_encode(); }
  | TRIGGER             { $$ = RCUC_FMD_TRIGGER_cmd_encode(); }
  | PULSER MODE ON      { $$ = RCUC_FMD_PULSER_MODE_cmd_encode(1); }
  | PULSER MODE OFF     { $$ = RCUC_FMD_PULSER_MODE_cmd_encode(0); }
  ;

l0_instruction
  : READ TRIGGER ZERO board_address	{ $$ = RCUC_FMD_L0_read_encode($4); }
  ;


%%
/* Code follows */
int
yyerror(const char* msg) 
{
  return RCUC_error(msg);
}

  
  
/*
 * EOF
 */
