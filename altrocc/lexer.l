/* -*- mode: c -*- */
/* 
 *
 * Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307 USA
 */
%{
/* Declarations */
#include <compiler.h>
#include <parser.h>
%}

/* Short hand patterns */
NAME		[_a-zA-Z][_A-Za-z0-9]*
FILENAME	[^ \t\n][^ \t\n]*
INTEGER		([0-9]+)|(0[Xx][0-9a-fA-F]+)
OFFSET		[-+]{INTEGER}

ZERO		[Zz][Ee][Rr][Oo]
WRITE		[Ww][Rr][Ii][Tt][Ee]
WORDS           [Ww][Oo][Rr][Dd][Ss]
WAIT		[Ww][Aa][Ii][Tt]
VOLTAGE         [Vv][Oo][Ll][Tt][Aa][Gg][Ee]
VFS             [Vv][Ff][Ss]
VFP             [Vv][Ff][Pp]
UNDER           [Uu][Nn][Dd][Ee][Rr]
TWO             [Tt][Ww][Oo]
TRIGGER		[Tt][Rr][Ii][Gg][Gg][Ee][Rr]
TIMEOUT         [Tt][Ii][Mm][Ee][Oo][Uu][Tt]
THRESHOLD       [Tt][Hh][Rr][Ee][Ss][Hh][Oo][Ll][Dd]
THIRD           [Tt][Hh][Rr][Ii][Dd]
TEST            [Tt][Ee][Ss][Tt]
TEMPERATURE     [Tt][Ee][Mm][Pp][Ee][Rr][Aa][Tt][Uu][Rr][Ee]
SUPPRESSION	[Ss][Uu][Pp][Pp][Rr][Ee][Ss][Ss][Ii][Oo][Nn]
STROBE          [Ss][Tt][Rr][Oo][Bb][Ee]
STATUS		[Ss][Tt][Aa][Tt][Uu][Ss]
SOFTWARE	[Ss][Oo][Ff][Tt][Ww][Aa][Rr][Ee]
SIZE		[Ss][Ii][Zz][Ee]
SHIFT           [Ss][Hh][Ii][Ff][Tt]
SECOND		[Ss][Ee][Cc][Oo][Nn][Dd]
SCAN            [Ss][Cc][Aa][Nn]
SAVE		[Ss][Aa][Vv][Ee]
SAMPLE          [Ss][Aa][Mm][Pp][Ll][Ee]
RESET		[Rr][Ee][Ss][Ee][Tt]
READOUT		[Rr][Ee][Aa][Dd][Oo][Uu][Tt]
READ		[Rr][Ee][Aa][Dd]
RCU		[Rr][Cc][Uu]
RANGE           [Rr][Aa][Nn][Gg][Ee]
PULSER          [Pp][Uu][Ll][Ss][Ee][Rr]
PRE		[Pp][Rr][Ee]
POWER		[Pp][Oo][Ww][Ee][Rr]
PEDESTAL	[Pp][Ee][Dd][Ee][Ss][Tt][Aa][Ll]
PATH		[Pp][Aa][Tt][Hh]
ONE             [Oo][Nn][Ee]
ON              [Oo][Nn]
OFF             [Oo][Ff][Ff]
MONITOR         [Mm][Oo][Nn][Ii][Tt][Oo][Rr]
MODE            [Mm][Oo][Dd][Ee]
LOOP		[Ll][Oo][Oo][Pp]
L3		[Ll]3
L2		[Ll]2
L1		[Ll]1
LENGTH          [Ll][Ee][Nn][Gg][Tt][Hh]
LATCH           [Ll][Aa][Tt][Cc][Hh]
K3		[Kk]3
K2		[Kk]2
K1		[Kk]1
JUMP		[Jj][Uu][Mm][Pp]
INVERT		[Ii][Nn][Vv][Ee][Rr][Tt]
INCREMENT	[Ii][Nn][Cc][Rr][Ee][Mm][Ee][Nn][Tt]
HOLD            [Hh][Oo][Ll][Dd]
GROUP		[Gg][Rr][Oo][Uu][Pp]
FIRST		[Ff][Ii][Rr][Ss][Tt]
FILTER		[Ff][Ii][Ll][Tt][Ee][Rr]
FMD             [Ff][Mm][Dd]
ERROR		[Ee][Rr][Rr][Oo][Rr]
EVENT           [Ee][Vv][Ee][Nn][Tt]
END		[Ee][Nn][Dd]
DIGITAL         [Dd][Ii][Gg][Ii][Tt][Aa][Ll]
DELAY           [Dd][Ee][Ll][Aa][Yy]
DATA            [Dd][Aa][Tt][Aa]
CURRENT         [Cc][Uu][Rr][Rr][Ee][Nn][Tt]
COUNTER		[Cc][Oo][Uu][Nn][Tt][Ee][Rr]
CONFIG		[Cc][Oo][Nn][Ff][Ii][Gg]
CLOCK           [Cc][Ll][Oo][Cc][Kk]
CHANGE          [Cc][Hh][Aa][Nn][Gg][Ee]
BUFFER		[Bb][Uu][Ff][Ff][Ee][Rr]
BROADCAST	[Bb][Rr][Oo][Aa][Dd][Cc][Aa][Ss][Tt]
BIAS            [Bb][Ii][Aa][Ss]
BC              [Bb][Cc]
BASELINE	[Bb][Aa][Ss][Ee][Ll][Ii][Nn][Ee]
ANALOG          [Aa][Nn][Aa][Ll][Oo][Gg]
ALTRO		[Aa][Ll][Tt][Rr][Oo]
ADDRESS		[Aa][Dd][Dd][Rr][Ee][Ss][Ss]
  
/* Start conditions */
%x COMMENT

/* Scanner rules */
%%
"#"			{ BEGIN(COMMENT); }
<COMMENT>[^*\n]*
<COMMENT>\n		{ RCUC_lineno_increment(); BEGIN(INITIAL); }

"1"[sS][tT]		{ return FIRST; }
"2"[nN][dD]		{ return SECOND; }
"3"[rR][dD]             { return THIRD; }
{ZERO}			{ return ZERO; }		
{WRITE}			{ return WRITE; }		
{WORDS}			{ return WORDS; }
{WAIT}			{ return WAIT; }	
{VOLTAGE}               { return VOLTAGE; }
{VFS}          		{ return VFS; }
{VFP}          		{ return VFP; }
{UNDER}			{ return UNDER; }
{TWO}                   { return TWO; }	
{TRIGGER}		{ return TRIGGER; }	
{TIMEOUT}               { return TIMEOUT; }
{THRESHOLD}             { return THRESHOLD; }
{THIRD}                 { return THIRD; }
{TEST}                  { return TEST; }
{TEMPERATURE}           { return TEMPERATURE; }
{SUPPRESSION}		{ return SUPPRESSION; }	
{STROBE}		{ return STROBE; }
{STATUS}		{ return STATUS; }	
{SOFTWARE}		{ return SOFTWARE; }	
{SIZE}			{ return SIZE; }		
{SHIFT}			{ return SHIFT; }		
{SECOND}		{ return SECOND; }
{SCAN}			{ return SCAN; }
{SAMPLE} 		{ return SAMPLE; }
{SAVE}			{ return SAVE; }		
{RESET}			{ return RESET; }		
{READOUT}		{ return READOUT; }	
{READ}			{ return READ; }		
{RCU}			{ return RCU; }		
{RANGE}			{ return RANGE; }	
{PULSER}		{ return PULSER; }	
{PRE}			{ return PRE; }		
{POWER}			{ return POWER; }		
{PEDESTAL}		{ return PEDESTAL; }	
{PATH}			{ return PATH; }		
{ONE}                   { return ONE; }
{ON}                    { return ON; }
{OFF}                   { return OFF; }
{MONITOR}               { return MONITOR; }
{MODE}			{ return MODE; }
{LOOP}			{ return LOOP; }		
{L3}			{ return L3; }		
{L2}			{ return L2; }		
{L1}			{ return L1; }	
{LENGTH}		{ return LENGTH; }	
{LATCH}                 { return LATCH; }
{K3}			{ return K3; }		
{K2}			{ return K2; }		
{K1}			{ return K1; }		
{JUMP}			{ return JUMP; }		
{INVERT}		{ return INVERT; }	
{INCREMENT}		{ return INCREMENT; }	
{HOLD}                  { return HOLD; }
{GROUP}			{ return GROUP; }		
{FMD}                   { return FMD; }
{FIRST}			{ return FIRST; }		
{FILTER}		{ return FILTER; }
{EVENT}			{ return EVENT; }
{ERROR}			{ return ERROR; }		
{END}			{ return END; }		
{DIGITAL}               { return DIGITAL; }
{DELAY}                 { return DELAY; }
{DATA}			{ return DATA; }
{CURRENT} 		{ return CURRENT; }
{COUNTER}		{ return COUNTER; }	
{CONFIG}		{ return CONFIG; }	
{CLOCK}                 { return CLOCK; }
{CHANGE}                { return CHANGE; }
{BUFFER}		{ return BUFFER; }	
{BROADCAST}		{ return BROADCAST; }	
{BIAS}                  { return BIAS; }
{BC}                    { return BC; }
{BASELINE}		{ return BASELINE; }	
{ANALOG}                { return ANALOG; }
{ALTRO}			{ return ALTRO; }		
{ADDRESS}		{ return ADDRESS; }	

"+"			{ return PLUS; }
"-"			{ return MINUS; }
":"			{ return COLON; }

{NAME}                  { RCUC_set_label(yytext, yyleng); return NAME; }
{INTEGER}		{ yylval = strtol(yytext, NULL, 0); return INTEGER; }
\n			{ RCUC_lineno_increment(); }
.			{ if (yytext[0] == EOF) return 0; }

%%
/* Code follows */ 
int
yywrap() 
{
  return 1;
}


/*
 * EOF
 */
